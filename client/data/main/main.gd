
extends Node

# Handle quit request
func _notification(what):
	if (what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST):
		# Stop server on quit (notify clients)
		print("Main quit")
