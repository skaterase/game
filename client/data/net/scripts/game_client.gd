
extends "game_peer.gd"


const Client = preload("res://data/libraries/net/client.gd")

# Game world
onready var world = get_node("/root/root_node/world")

# Reference to player
var player

# Client name
var name = Marshalls.variant_to_base64(rand_seed(OS.get_ticks_msec() * OS.get_frames_drawn())[0]).replace("/", "").replace("+","").replace("=","")

# Snapshots received from server (snapshot buffer)
var snapshots = null

# Current player state used to check if state changed to avoid sending data (and save bandwidth)
var state = {
	input = null,
	rotation = null
}

# Constructor
func _init(config).(config):
	peer = Client.new(config)
	_connect_handlers()

# Reset values
func reset():
	if (world != null && player != null):
		world.reset()
	player = null
	state.input = null
	state.rotation = null
	snapshots = null

# Start game client
func start():
	while(! peer.start()):
		peer.port += 1
		if (peer.port < 1 || int(peer.port) > 65535):
			print("Invalid port ", str(peer.port))
			break
	if (peer.is_running):
		peer.connect_to_server(name)
	else:
		print("Unable to connect to server. Failed to bind port.")

# Handle client connected to server
func _on_connection_connected(connection, hail):
	print("Server connection connected")
	
	# Tell the server that we want the world! THE ENTIRE WORLD!
	get_world(connection)
	
	# TODO: Wait to spawn until world is loaded
	# ...
	
	# Spawn self
	player = world.spawn_self(hail)
	set_fixed_process(true)
	
	
	

# Handle disconnect from server
func _on_connection_disconnected(connection, reason):
	print("Server disconnected")
	var name = connection.name
	if (player != null):
		name = player.get_name()
		player = null
	world.destroy(name)
	stop()

# Handle received packet
# packet = [ connection, channel, message ]
func _receive(delta):
	for packet in peer.read_messages():
		if (packet[1] == peer.Channel.app_unreliable + Channel.update):
			var message = packet[2] 
			var tick = message[1]
			message.pop_front()
			message.pop_front()
			handle_update(message, tick)
		elif(packet[1] == peer.Channel.app_unreliable + Channel.build_ground):
			var message = packet[2] 
			var vec2_pos = message[1]
			var build_node = get_node("/root/root_node/GUI/building")
			build_node.client_place_ground(vec2_pos)
		elif(packet[1] == peer.Channel.app_unreliable + Channel.remove_ground):
			var message = packet[2] 
			var vec2_pos = message[1]
			var build_node = get_node("/root/root_node/GUI/building")
			build_node.client_remove_ground(vec2_pos)
		elif(packet[1] == peer.Channel.app_unreliable + Channel.build_wall):
			var message = packet[2] 
			var vec2_pos = message[1]
			var build_node = get_node("/root/root_node/GUI/building")
			build_node.client_place_wall(vec2_pos)
		elif(packet[1] == peer.Channel.app_unreliable + Channel.remove_wall):
			var message = packet[2] 
			var vec2_pos = message[1]
			var build_node = get_node("/root/root_node/GUI/building")
			build_node.client_remove_wall(vec2_pos)
		elif(packet[1] == peer.Channel.app_unreliable + Channel.build_door):
			var message = packet[2] 
			var vec2_pos = message[1]
			var build_node = get_node("/root/root_node/GUI/building")
			build_node.client_place_door(vec2_pos)
		elif(packet[1] == peer.Channel.app_unreliable + Channel.load_world):
			print("CLIENT: Loading World Message Received")
			var world_array = packet[2] 
			world_array.pop_front()
			var build_node = get_node("/root/root_node/GUI/building")
			build_node.client_load_world(world_array)
		else:
			print("Type is not update")

# Send periodic updates to server
func _send(delta):
	if ((tick % 2) == 0):
		# Assemble update and send
		var channel = peer.Channel.app_unreliable + Channel.update
		var message = _assemble_server_update()
		var connection = peer.get_server_connection()
		peer.send_message(message, connection, channel)

# Used by client to request the world from the server after connecting
func get_world(connection):
	peer.send_message([], connection, peer.Channel.app_unreliable + Channel.load_world)

# Send array containing Vector2 position for the tile being placed
func add_ground(vec2_pos):
	var channel = peer.Channel.app_unreliable + Channel.build_ground 
	var connection = peer.get_server_connection()
	peer.send_message(vec2_pos, connection, channel)

# Send array containing Vector2 position for the tile being removed
func remove_ground(vec2_pos):
	var channel = peer.Channel.app_unreliable + Channel.remove_ground 
	var connection = peer.get_server_connection()
	peer.send_message(vec2_pos, connection, channel)

# Send array containing Vector2 position for the tile being placed
func add_wall(vec2_pos):
	var channel = peer.Channel.app_unreliable + Channel.build_wall 
	var connection = peer.get_server_connection()
	peer.send_message(vec2_pos, connection, channel)

# Send array containing Vector2 position for the tile being removed
func remove_wall(vec2_pos):
	var channel = peer.Channel.app_unreliable + Channel.remove_wall
	var connection = peer.get_server_connection()
	peer.send_message(vec2_pos, connection, channel)

# Send array containing Vector2 position for the tile being placed
func add_door(vec2_pos):
	var channel = peer.Channel.app_unreliable + Channel.build_door 
	var connection = peer.get_server_connection()
	peer.send_message(vec2_pos, connection, channel)

# Assemble periodic update packet
func _assemble_server_update():
	# Prepare packet array
	var packet = [
		# Bit encoded state change:
		#	Bit 1 == 0 meaning no change
		# 	Bit 1 == 1 meaning input changed
		# 	Bit 2 == 1 meaning rotation changed
		0
	]
	# Send input if changed
	if (state.input != player.input):
		state.input = player.input
		packet.append(player.input)
		packet[0] += 1
	# Send rotation if changed
	if (state.rotation != player.rotation):
		state.rotation = player.rotation
		packet.append(player.rotation)
		packet[0] += 1 << 1
	
	return packet

# Update base class and apply snapshots
func _update(delta):
	if (snapshots == null || snapshots.empty()):
		# Not received the first snapshot yet or snapshot buffer empty so nothing to do
		return
	
	# Apply snapshot if tick is divisible by 3
	if (tick % 3 == 0):
		if (snapshots.has(tick - 3)):
			apply_snapshot(tick - 3)
			snapshots.erase(tick - 3)
		elif (snapshots.has(tick - 6)):
			apply_snapshot(tick - 6)
			snapshots.erase(tick - 6)

# Apply game state snapshot from server
func apply_snapshot(tick):
	for state in snapshots[tick]:
		var name = state[0]
		
		if (name == null || name == ""):
			continue
		
		var entity = null
		
		# Identify node by name
		if (world.has_node(name)):
			entity = world.get_node(name)
		elif (self.name == name):
			entity = player
		else:
			entity = world.spawn_other_player(name)
		
		# Apply state
		entity.set_state({
			input = state[1],
			position = state[2],
			rotation = state[3],
			velocity = state[4]
		})

# Handle update from server
func handle_update(update, tick):
	# Server tick
	if (snapshots == null):
		# First snapshot received (player already spawned at this point)
		print("First snapshot received at tick ", str(self.tick), ". Syncing to server tick ", str(tick))
		# Initialize snapshot buffer
		snapshots = {}
		# Synchronize client tick with server tick
		self.tick = tick
	
	# Append snapshot to snapshot buffer
	snapshots[tick] = update
	
	# Network latency varies so the client and server tick won't stay synchronized
	var skew = self.tick - tick
	
	# Resync client tick with server tick if skew is more than 2 ticks
	if (abs(skew) >= 3 && tick % 3 == 0):
		print("Tick skew is ", str(skew), ". Resyncing to server tick ", str(tick), " from ", str(self.tick))
		self.tick = tick
		# Clean up snapshots that are too old
		for t in snapshots.keys():
			if (t < tick - 6):
				print("Discarding old snapshot ", str(t), " at tick ", str(tick))
				snapshots.erase(t)

func chat(msg):
	if (peer == null):
		print("Chat unavailable. Peer not running.")
	else:
		peer.broadcast(msg, peer.Channel.app_reliable + Channel.chat)
