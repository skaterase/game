
extends Node

# Application message types
var Channel = {
	update = 1,
	chat = 2,
	build = 3,
	info = 4,
	build_ground = 5,
	remove_ground = 6,
	build_wall = 7,
	remove_wall = 8,
	build_door = 9, # we do not need a separate one for remove door, as we use the same as remove_wall for that
	load_world = 10
}
# The highest signed int is 0x7fffffff but to make the tick modulo 3
# continuous before and after wrapping 0x7ffffffd is used.
# Modulo 3 because it's a factor in both 60/20 = 3 and 60/10 = 6 where
# 60 is the fixed fps, and 20 and 10 are the server (good and bad) send rates.
# This means the server will be able to send updates at fixed intervals to
# connections with good and bad send rates.
const MAX_TICK = 0x7ffffffd

var peer
var config
var tick

func _init(config):
	self.config = config
	self.tick = 0

func _connect_handlers():
	peer.connect("start", self, "_on_peer_start", [], CONNECT_DEFERRED)
	peer.connect("stop", self, "_on_peer_stop", [], CONNECT_DEFERRED)
	peer.connect("connected", self, "_on_connection_connected", [], CONNECT_DEFERRED)
	peer.connect("disconnected", self, "_on_connection_disconnected", [], CONNECT_DEFERRED)
	peer.connect("accepted", self, "_on_connection_accepted", [], CONNECT_DEFERRED)

func _on_peer_start():
	pass

func _on_peer_stop(reason):
	pass

func _on_connection_connected(connection, hail):
	pass

func _on_connection_accepted(connection):
	pass

func _on_connection_disconnected(connection, reason):
	pass


# Handle quit request
func _notification(what):
	if (what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST):
		# Stop server on quit (notify clients)
		stop()

# Ready
func _ready():
	start()

func _fixed_process(delta):
	tick += 1
	# Wrap tick
	if (tick > MAX_TICK):
		tick = 1
	# Receive packets
	_receive(delta)
	# Send packets
	_send(delta)
	
	_update(delta)

func _receive(delta):
	pass

func _send(delta):
	pass

func _update(delta):
	pass

# Stop game peer
func stop():
	set_fixed_process(false)
	peer.stop()
