
extends "game_peer.gd"


const Server = preload("res://data/libraries/net/server.gd")

# Game world
onready var world = get_node("/root/root_node/world")

# Constructor
func _init(config).(config):
	peer = Server.new(config)
	_connect_handlers()

# Reset game server
func reset():
	tick = 0
	if (world != null):
		world.reset()
	.reset()

func start():
	peer.start()
	if (peer.is_running):
		set_fixed_process(true)

# Handle incoming packets
# packet = [ connection, channel, message ]
func _receive(delta):
	for message in peer.read_messages():
		var connection = message[0]
		var channel = message[1]
		var content = message[2]
		
		if (channel == peer.Channel.app_unreliable + Channel.update):
			_handle_update(connection, content)
		elif(channel == peer.Channel.app_unreliable + Channel.build_ground):
			add_ground(connection, content)
		elif(channel == peer.Channel.app_unreliable + Channel.remove_ground):
			remove_ground(connection, content)
		elif(channel == peer.Channel.app_unreliable + Channel.build_wall):
			add_wall(connection, content)
		elif(channel == peer.Channel.app_unreliable + Channel.remove_wall):
			remove_wall(connection, content)
		elif(channel == peer.Channel.app_unreliable + Channel.build_door):
			add_door(connection, content)
		elif(channel == peer.Channel.app_unreliable + Channel.load_world):
			send_world(connection)
		else:
			print("Game server received unhandled message from ", connection.name, " on channel ", channel)

# Add to server and forward changes to clients connected
func add_ground(connection, content):
	var vec2_pos = content[1] 
	get_node("/root/root_node/GUI/building").server_place_ground(vec2_pos)
	content.pop_front() # Removes initial channel (as it re-attached after send_message)
	
	# Prepare forwarding
	for client in peer.get_connections():
		if(connection.name == client.name):
			continue
		peer.send_message(content, client, peer.Channel.app_unreliable + Channel.build_ground)

# Remove from server and forward changes to clients connected
func remove_ground(connection, content):
	var vec2_pos = content[1] 
	get_node("/root/root_node/GUI/building").server_remove_ground(vec2_pos)
	content.pop_front() # Removes initial channel (as it re-attached after send_message)
	
	# Prepare forwarding
	for client in peer.get_connections():
		if(connection.name == client.name):
			continue
		peer.send_message(content, client, peer.Channel.app_unreliable + Channel.remove_ground)

# Add to server and forward changes to clients connected
func add_wall(connection, content):
	var vec2_pos = content[1] 
	get_node("/root/root_node/GUI/building").server_place_wall(vec2_pos)
	content.pop_front() # Removes initial channel (as it re-attached after send_message)
	
	# Prepare forwarding
	for client in peer.get_connections():
		if(connection.name == client.name):
			continue
		peer.send_message(content, client, peer.Channel.app_unreliable + Channel.build_wall)

# Remove from server and forward changes to clients connected
func remove_wall(connection, content):
	var vec2_pos = content[1] 
	get_node("/root/root_node/GUI/building").server_remove_wall(vec2_pos)
	content.pop_front() # Removes initial channel (as it re-attached after send_message)
	
	# Prepare forwarding
	for client in peer.get_connections():
		if(connection.name == client.name):
			continue
		peer.send_message(content, client, peer.Channel.app_unreliable + Channel.remove_wall)

# Add to server and forward changes to clients connected
func add_door(connection, content):
	var vec2_pos = content[1] 
	get_node("/root/root_node/GUI/building").server_place_door(vec2_pos)
	content.pop_front() # Removes initial channel (as it re-attached after send_message)
	
	# Prepare forwarding
	for client in peer.get_connections():
		if(connection.name == client.name):
			continue
		peer.send_message(content, client, peer.Channel.app_unreliable + Channel.build_door)


# Send the current world to the player that requested it
func send_world(connection):
	var world_array = get_node("/root/root_node/GUI/building").get_world()
	peer.send_message(world_array, connection, peer.Channel.app_unreliable + Channel.load_world)


# Send periodic update to clients
#
# Send rate (BAD ): 10 per second: 1 / 10 = 0.1 second interval (100 ms)
# Send rate (GOOD): 20 per second: 1 / 20 = 0.05 second interval (50 ms)
# 60 ticks per second / 10 = 6 ticks per update (BAD RATE)
# 60 ticks per second / 20 = 3 ticks per update (GOOD RATE)
# 3 % 3 == 0 && 3 % 2 != 0 (GOOD RATE ONLY)
# 6 % 3 == 0 && 6 % 2 == 0 (BAD RATE ONLY)
func _send(delta):
	if (tick % 3 == 0):
		_update_clients()

func _update_clients():
	#var update_bad_connections = false
	# Determine if connections with bad send rate shoulde be update as well
	# if ((tick / 3) % 2 == 0):
	#	update_bad_connections = true
	
	var states = _get_player_states()
	
	for client in peer.get_connections():
		# if (!update_bad_connections && client.flow_control.is_mode_bad()):
		# 	break
		var packet = _assemble_client_packet(client, states)
		_send_update(client, packet)

func _send_update(client, packet):
	peer.send_message(packet, client, peer.Channel.app_unreliable + Channel.update)

# Return array of all player states
func _get_player_states():
	var states = []
	for player in get_tree().get_nodes_in_group("players"):
		states.append([
			player.get_name(),
			player.input,
			player.position,
			player.rotation,
			player.velocity
		])
	return states

# Return packet with states tailored to given client
func _assemble_client_packet(client, states):
	# Get client's player
	var client_player = world.get_node(client.name)
	
	# Create update packet including the server (fixed) frame tick
	var packet = [tick]
	
	for state in states:
		# Only append state if distance between players is less than 4000 pixels
		var distance = client_player.position.distance_to(state[2])
		if (distance < 4000):
			packet.append(state)
	
	return packet

# Handle connection to server
func _on_connection_connected(connection, hail):
	print("Game server: _on_connection connected: ", hail)
	# Spawn player and add connection to connection list or
	# return connect error message with reason
	if (hail != null && hail == ""):
		hail = str(rand_seed(OS.get_ticks_msec() * OS.get_frames_drawn())[0])
	connection.name = hail
	if (world.has_node(connection.name)):
		print("Disapproved ", connection.name)
		connection.disapprove("Name taken: " + connection.name)
	else:
		print("Approved ", connection.name)
		connection.approve(connection.name)
		world.spawn_server_player(connection.name)


# Handle disconnection from server
func _on_connection_disconnected(connection, reason):
	print(connection.name, " disconnected: ", str(reason))
	world.destroy(connection.name)

# Handle update from connection
func _handle_update(connection, update):
	# TODO Don't update using older states
	# Use the same bit_sequence_is_newer function as in connection.gd
	# (because of sequence number wrap around)
#	if (connection.remote_seq > most_recent_update):
#		most_recent_update = connection.remote_seq
#	else:
#		return
	if (!world.has_node(connection.name)):
		print("Connection name not in world")
		return
	
	var player = world.get_node(connection.name)
	
	# Bit encoded state information
	var bits = update[1]
	if (bits == 0):
		return
	
	# Decode information and update player accordingly
	# See game_client.gd for encoding
	if (bits == 1):
		player.input = update[2]
	elif (bits == 2):
		player.rotation = update[2]
	elif (bits == 3):
		player.input = update[2]
		player.rotation = update[3]
