
extends Node

# Buttons
onready var button_close = find_node("button_close")
onready var button_return_main_menu = find_node("button_return_main_menu")
onready var button_quit_game = find_node("button_quit_game")


# Building
onready var node_building = get_node("/root/root_node/GUI/building")

func _ready():
	button_close.connect("pressed", self, "_on_close_pressed")
	button_return_main_menu.connect("pressed", self, "_on_return_main_menu_pressed")
	button_quit_game.connect("pressed", self, "_on_quit_pressed")
	
	self.connect("enter_tree", node_building, "on_button_hovering", [ true ])
	# self.connect("exit_tree", node_building, "on_button_hovering", [ false ])
	pass

func _on_close_pressed():
	#self.queue_free() # Remove self (instance)
	self.queue_free()
	pass

func _on_return_main_menu_pressed():
	# TODO NETWORKING: Inform server that the player is leaving
	
	# Close scene and return to main menu
	get_tree().change_scene("res://data/main_menu/main_menu.tscn")
	pass

# Quit Game
func _on_quit_pressed():
	get_tree().quit()
	pass