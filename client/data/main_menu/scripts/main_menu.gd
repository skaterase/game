
extends Node

# MAIN MENU
# Containers
onready var container_menu = get_node("container_menu")

# Buttons
onready var solo_button = find_node("button_solo")
onready var join_server_button = find_node("button_join_server")
onready var host_server_button = find_node("button_host_server")
onready var settings_button = find_node("button_settings")
onready var quit_button = find_node("button_quit")

# JOIN SERVER MENU
# Containers
onready var container_join_server = get_node("container_join_server")

# Buttons
onready var cancel_button = find_node("button_cancel_join_server")
onready var connect_button = find_node("button_connect_server")

# TextField
onready var address_textfield = find_node("LineEdit")

# Configuration
onready var config = get_node("/root/global").config

func _ready(): 
	
	# Singletons MAIN MENU
	solo_button.connect("pressed", self, "_on_solo_pressed")
	join_server_button.connect("pressed", self, "_on_join_server_pressed")
	host_server_button.connect("pressed", self, "_on_host_server_pressed")
	settings_button.connect("pressed", self, "_on_settings_pressed")
	quit_button.connect("pressed", self, "_on_quit_pressed")
	
	# Singletons JOIN SERVER MENU
	cancel_button.connect("pressed", self, "_on_cancel_join_server_pressed")
	connect_button.connect("pressed", self, "_on_connect_server_pressed")


# MAIN MENU PRESSES
func _on_solo_pressed():
	get_node("/root/global").play_solo()

func _on_join_server_pressed():
	# Go to the connect server menu
	container_menu.hide()
	container_join_server.show()
	# Initialize with configured values for server ip and port
	var address = config.client.server_ip + ":" + str(config.client.server_port)
	address_textfield.set_text(address)

func _on_host_server_pressed():
	get_node("/root/global").host_server()

func _on_settings_pressed():
	# Do stuff
	pass

func _on_quit_pressed():
	get_tree().quit()
	pass

# JOIN SERVER MENU PRESSES
func _on_cancel_join_server_pressed():
	# Cancel and go back
	container_menu.show()
	container_join_server.hide()
	pass

func _on_connect_server_pressed():
	var address = address_textfield.get_text().split(":", true)
	var ip = address[0]
	var port = address[1]
	if (int(port) != 0):
		address_textfield.set_text("Invalid port: " + port)
	get_node("/root/global").connect_to_server(ip, port)
