# Hierarchy: player_other.gd => object_alive.gd => object_movable.gd => RigidBody2D
extends RigidBody2D

# Player variables
export var sprint_speed = 2.5 # animation speed when sprinting
var position = Vector2(0, 0)
var rotation = 0.0
var velocity = Vector2(0, 0)

# Player animations
onready var animation = get_node("player_sprites/base/animated_sprite/animation_player")

# Used for interpolation
const EPSILON = 0.0005
var interpolation_period = 1.0 / 10.0 # 100 milliseconds
var interpolation_time = 0.0
var interpolate_from_position = Vector2(0, 0)
var interpolate_to_position = Vector2(0, 0)
var interpolate_to_rotation = 0.0
var interpolate_from_rotation = 0.0
var is_state_updated = false # if true then update interpolation points

func _ready():
	# We don't need collision on other client players because server runs simulation
	# and collision interferes with interpolation
	remove_child(get_node("CollisionShape2D"))
	# Other client does not have camera, only client player
	remove_child(get_node("camera"))

# Integrate forces
func _integrate_forces(state):
	if (is_state_updated):
		interpolate_from_position = state.get_transform().get_origin()
		interpolate_from_rotation = state.get_transform().get_rotation()
		interpolate_to_position = position
		interpolate_to_rotation = rotation
		interpolation_time = 0
		is_state_updated = false
	
	interpolation_time += state.get_step()
	if (interpolation_time < interpolation_period):
		var alpha = interpolation_time / interpolation_period
		# Interpolate position and rotation
		var pos = interpolate_from_position.linear_interpolate(interpolate_to_position, alpha)
		var rot = slerp_rot(interpolate_from_rotation, interpolate_to_rotation, alpha)
		state.set_transform(Matrix32(rot, pos))
	
	animate()

func animate():
	# If we are moving and the walk animation is not playing; we play it!
	if(self.is_moving() && !animation.get_current_animation() == "walk"):
		animation.play("walk")
	elif(!self.is_moving() && !animation.get_current_animation() == "idle"):
		animation.play("idle")

# Return true if player moving (based on velocity)
func is_moving():
	return abs(velocity.x) > 10 || abs(velocity.y) > 10

# Set up state variables
func set_state(state):
	if (state.position != null):
		position = state.position
	if (state.rotation != null):
		rotation = state.rotation
	if (state.velocity != null):
		velocity = state.velocity
	if (state.input != null):
		var is_sprint_pressed = 0x10 & state.input
		var is_moving = state.input & 0xf
		if (is_sprint_pressed && is_moving):
			animation.set_speed(sprint_speed) 
		else:
			animation.set_speed(1)
	is_state_updated = true

# Lerp vector
func lerp_pos(v1, v2, alpha):
	return v1 * alpha + v2 * (1.0 - alpha)

# Spherically linear interpolation of rotation
func slerp_rot(r1, r2, alpha):
	var v1 = Vector2(cos(r1), sin(r1))
	var v2 = Vector2(cos(r2), sin(r2))
	var v = slerp(v1, v2, alpha)
	return atan2(v.y, v.x)

# Spherical linear interpolation of two 2D vectors
func slerp(v1, v2, alpha):
	var cos_angle = clamp(v1.dot(v2), -1.0, 1.0)
	
	if (cos_angle > 1.0 - EPSILON):
		return lerp_pos(v1, v2, alpha).normalized()
	
	var angle = acos(cos_angle)
	var angle_alpha = angle * alpha
	var v3 = (v2 - (cos_angle * v1)).normalized()
	return v1 * cos(angle_alpha) + v3 * sin(angle_alpha)
