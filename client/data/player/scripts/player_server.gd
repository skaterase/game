# Hierarchy: player_other.gd => object_alive.gd => object_movable.gd => RigidBody2D
extends "res://data/scripts/object_alive.gd"

export var speed_default = 1000.0 # save default speed
export var sprint_speed = 2.5 # speed multiplier when sprinting

# This is sent to clients
var rotation = 0.0 setget _set_rotation
var velocity = Vector2(0, 0)
var position = Vector2(0, 0)

func _set_rotation(rot):
	rotation = rot
	is_rotation_set = false

# State received from client
var input = 0

# Used to set rotation only once after state update from client
var is_rotation_set = false

func _ready():
	# Players spawned on the server does not need a camera
	remove_child(get_node("camera"))
	set_fixed_process(true)

# Process fixed frame
func _fixed_process(delta):
	# Get the state of the player so we can send it to the clients
	velocity = get_linear_velocity()
	position = get_pos()
	# Only update rotation ONCE (after state update)
	if (! is_rotation_set):
		set_rot(rotation)
		is_rotation_set = true

# Apply force based on input from client
func apply_force(state):
	if (input == null):
		return
	else:
		force = Vector2(0, 0)
	
	var move_up = input & 1
	var move_down = input & 1 << 1
	var move_left = input & 1 << 2
	var move_right = input & 1 << 3
	var sprint = input & 1 << 4
	var is_moving = input & 0xf
	var is_sprinting = sprint && is_moving
	
	if(is_sprinting):
		speed = speed_default * sprint_speed
	else:
		speed = speed_default
	
	if (! is_moving):
		return
	
	if (move_up):
		force += FORCE.UP
	elif(move_down):
		force += FORCE.DOWN
	if(move_left):
		force += FORCE.LEFT
	elif(move_right):
		force += FORCE.RIGHT
