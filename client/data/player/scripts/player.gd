extends RigidBody2D

# Player variables
export var sprint_speed = 2.5 # Animation speed when sprinting
var position = Vector2(0, 0) # Player world position
var velocity = Vector2(0, 0) # Player velocity used for animation
var input = 0 # Bit encoded input
var mouse_position = Vector2(0, 0)
var rotation = 0.0

# Player animations
onready var animation = get_node("player_sprites/base/animated_sprite/animation_player")

# Used for interpolating player position
var interpolation_period = 6 # nr of ticks in interpolation
var interpolation_ticks = 0.0
var interpolate_from_position = Vector2(0, 0)
var interpolate_to_position = Vector2(0, 0)
var is_state_updated = true

# Initialize
func _ready():
	# We don't need collision on client player because server runs simulation
	# and collision interferes with interpolation
	remove_child(get_node("CollisionShape2D"))
	# Disable internal force integration (we do it ourselves)
	set_use_custom_integrator(true)
	# Sample input in fixed process
	set_fixed_process(true)
	# Subscribe to receive console enable/disable
	get_node("/root/root_node/GUI/console").connect("console", self, "on_console")

# Handle console enabled/disabled
# If console disabled we turn off processing input and vice versa
func on_console(is_enabled):
	if (is_enabled):
		# Set input to 0 to avoid any input being sent to the server while console enabled
		# TODO should stop sending updates while console enabled to save bandwidth
		input = 0
		set_fixed_process(false)
	else:
		set_fixed_process(true)

# Integrate forces (move player)
func _integrate_forces(state):
	# If the state is updated we need to set up interpolation start and end point
	if (is_state_updated):
		interpolate_from_position = state.get_transform().get_origin()
		interpolate_to_position = position
		interpolation_ticks = 0
		is_state_updated = false
	
	mouse_position = get_global_mouse_pos()
	
	# If within interpolation period we transform position stepwise on the interpolation line
	if (interpolation_ticks < interpolation_period):
		interpolation_ticks += 1.0
		var alpha = interpolation_ticks / interpolation_period
		var pos = interpolate_to_position
		if (alpha == 1):
			pass
		else:
			# Calculate position to be the point alpha percent along the line between current position and target position
			pos = interpolate_from_position.linear_interpolate(interpolate_to_position, alpha)
		# Rotate to face mouse position directly (not from server state)
		var rot = (mouse_position - pos).angle()
		# Update rotation and position
		state.set_transform(Matrix32(rot, pos))
	else:
		if (interpolation_ticks == interpolation_period):
			interpolation_ticks += 1.0
			# Extrapolate
			state.set_linear_velocity(velocity)
		pass

# Set state
func set_state(state):
	if (state.position != null):
		position = state.position
	if (state.velocity != null):
		velocity = state.velocity
	
	is_state_updated = true

# Process fixed frame
func _fixed_process(delta):
	# Sample input
	var move_left = Input.is_action_pressed("move_left")
	var move_right = Input.is_action_pressed("move_right")
	var move_up = Input.is_action_pressed("move_up")
	var move_down = Input.is_action_pressed("move_down")
	var sprint = Input.is_action_pressed("sprint")
	
	# Bit encode input
	input = 0
	# If W is pressed, Move Up
	if (move_up and not move_down):
		input += 1
	# If S is pressed, Move Down
	if (move_down and not move_up):
		input |= 1 << 1
	# If A is pressed, Move Left
	if(move_left and not move_right):
		input |= 1 << 2
	# If D is pressed, Move Right
	if (move_right and not move_left):
		input |= 1 << 3
	# Set animation speed based on input
	if (sprint):
		input |= 1 << 4
	
	# Determine if we are moving
	var is_moving = input & 0xf
	
	# Set animation speed based on input
	if (sprint && is_moving):
		animation.set_speed(sprint_speed)
	else:
		animation.set_speed(1)
	
	rotation = (mouse_position - get_pos()).angle()
	
	animate()

# Animate player
func animate():
	# If we are moving and the walk animation is not playing; we play it!
	if(is_moving() && !animation.get_current_animation() == "walk"):
		animation.play("walk")
	elif(!is_moving() && !animation.get_current_animation() == "idle"):
		animation.play("idle")

# Return true if player has a certain threshold of velocity
func is_moving():
	# Has to be over 10 in value, otherwise the character will appear to be moving almost regardless how "still" it is
	return abs(velocity.x) > 10 || abs(velocity.y) > 10
