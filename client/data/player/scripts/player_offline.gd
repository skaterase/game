# Hierarchy: player_offline.gd => object_movable.gd => RigidBody2D
extends "res://data/scripts/object_movable.gd"

# Player variables
export var speed_default = 1000.0 # save default speed
export var sprint_speed = 2.5 # Animation speed when sprinting

var position = Vector2(0, 0) # Player world position
var velocity = Vector2(0, 0) # Player velocity used for animation
var mouse_position = Vector2(0, 0)

# Player animations
onready var animation = get_node("player_sprites/base/animated_sprite/animation_player")

# We don't apply force when console is enabled
var console_enabled = false

# Initialize
func _ready():
	# Enable fixed processing (used for rotation and animation)
	set_fixed_process(true)
	# Subscribe to receive console enable/disable signals
	get_node("/root/root_node/GUI/console").connect("console", self, "on_console")

# Handle console enabled/disabled
func on_console(is_enabled):
	console_enabled = is_enabled

# Apply force based on input from client
func apply_force(state):
	# Reset force
	force = Vector2(0, 0)
	
	# Do nothing if console is enabled
	if (console_enabled):
		return
	
	# Sample input
	var move_left = Input.is_action_pressed("move_left")
	var move_right = Input.is_action_pressed("move_right")
	var move_up = Input.is_action_pressed("move_up")
	var move_down = Input.is_action_pressed("move_down")
	var sprint = Input.is_action_pressed("sprint")
	
	if (move_right and move_left):
		move_right = false
		move_left = false
	
	if (move_up and move_down):
		move_up = false
		move_down = false
	
	var is_moving = move_down || move_left || move_right || move_up
	var is_sprinting = sprint && is_moving
	
	# Set movement speed
	if(is_sprinting):
		animation.set_speed(sprint_speed)
		speed = speed_default * sprint_speed
	else:
		animation.set_speed(1)
		speed = speed_default
	
	# Don't need to apply force if we are not moving
	if (! is_moving):
		return
	
	# Apply force
	if (move_up):
		force += FORCE.UP
	elif(move_down):
		force += FORCE.DOWN
	if(move_left):
		force += FORCE.LEFT
	elif(move_right):
		force += FORCE.RIGHT

# Process fixed frame
func _fixed_process(delta):
	# Look towards mouse_position
	mouse_position = get_global_mouse_pos()
	# The following two lines are not necessary
	velocity = get_linear_velocity()
	position = get_pos()
	
	look_at(mouse_position)
	
	animate()

# Animate player
func animate():
	# If we are moving and the walk animation is not playing; we play it!
	if(is_moving() && animation.get_current_animation() != "walk"):
		animation.play("walk")
	elif(!is_moving() && animation.get_current_animation() != "idle"):
		animation.play("idle")

# Return true if player has a certain threshold of velocity
func is_moving():
	# Has to be over 10 in value, otherwise the character will appear to be moving almost regardless how "still" it is
	return abs(velocity.x) > 10 || abs(velocity.y) > 10
