# ------------------------------------------------------------------------------
# Flow control for network connections
#
# Controls packet send rate based on Round Trip Time (aka ping)
# ------------------------------------------------------------------------------
extends Reference

const MODE = {
	BAD = 0,
	GOOD = 1
}


var config
var mode # mode
var good_conditions_accumulator # for accumulating time in good mode
var penalty_reduction_accumulator # for accumulating time 
var penalty_time # when rtt is passing threshold there may be a penalized if badmode lasted 

func _init(config):
	if (config == null):
		config = DEFAULT_CONFIG
	else:
		self.config = config
	reset()

# Reset data
func reset():
	mode = MODE.BAD
	good_conditions_accumulator = 0.0
	penalty_reduction_accumulator = 0.0
	penalty_time = config.penalty_time

# Get send rate
func get_send_rate():
	if (mode == MODE.GOOD):
		return config.good_rate
	else:
		return config.bad_rate

# Update send rate
func update(delta, rtt):
	if (mode == MODE.GOOD):
		if (rtt > config.rtt_threshold):
			print("Flow control: Dropping to bad mode (", str(bad_rate), ") packets per second")
			mode = MODE.BAD
			# Double penalty time if less then 10 seconds in good mode before dropping to bad mode.
			# Next time treshold is crossed in opposite direction (bad to good) you have to
			# stay penalty time below threshold before changing to good mode.
			if (good_conditions_accumulator < 10.0 && penalty_time < 60.0):
				penalty_time *= 2.0
				if (penalty_time > 60.0):
					penalty_time = 60.0
			good_conditions_accumulator = 0.0
			penalty_reduction_accumulator = 0.0
			return
			
		good_conditions_accumulator += delta
		penalty_reduction_accumulator += delta
		
		# Halve the penalty time every 10 secs in good mode
		if (penalty_reduction_accumulator > 10.0 && penalty_time > 1.0):
			penalty_time /= 2.0
			if (penalty_time < 1.0):
				penalty_time = 1.0
			penalty_reduction_accumulator = 0.0
	
	if (mode == MODE.BAD):
		if (rtt <= config.rtt_threshold):
			good_conditions_accumulator += delta
		else:
			good_conditions_accumulator = 0.0
		
		# Change to good mode if good conditions for more than penalty time
		if (good_conditions_accumulator > penalty_time):
			print("Flow control: Upgrading to good mode ", str(config.good_rate), " packets per second")
			mode = MODE.GOOD
			good_conditions_accumulator = 0.0
			penalty_reduction_accumulator = 0.0

# Return true if flow control is in BAD mode
func is_mode_bad():
	return mode == MODE.BAD
