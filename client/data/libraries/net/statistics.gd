
extends Reference

# Network statistics
var sent_packets = 0 # total sent packets
var received_packets = 0 # total received packets
var lost_packets = 0 # total lost packets
var acked_packets = 0 # total acked packets
var resent_packets = 0 # total resent packets
var packet_errors = 0 # total packet errors

func to_string():
	return "Received packets: " + str(received_packets) + "\n" + \
	"Sent packets: " + str(sent_packets) + "\n"