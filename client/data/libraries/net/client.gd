# -----------------------------------------------------------------------------
# Network client
#
# Inheritance: Reference > peer.gd > client.gd
# -----------------------------------------------------------------------------
extends "peer.gd"

# Constructor
func _init(config).(config):
	# Set listening port from config
	port = config.client.listening_port
	name = "client"

func connect_to_server(hail):
	var ip = config.client.server_ip
	var port = config.client.server_port
	_connect_to(ip, port, hail)

func get_statistics():
	var server = get_server_connection()
	if (server != null):
		server.statistics
 
func get_server_connection():
	if (connections.empty()):
		return null
	return connections[0]
