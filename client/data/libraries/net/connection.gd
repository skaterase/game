# ------------------------------------------------------------------------------
# Reliable connection
#
# Abstraction for a reliable connection over UDP
# ------------------------------------------------------------------------------
extends Reference

const ReliableSendChannel = preload("reliable_send_channel.gd")
const UnreliableSendChannel = preload("unreliable_send_channel.gd")
const UnreliableReceiveChannel = preload("unreliable_receive_channel.gd")
const ReliableReceiveChannel = preload("reliable_receive_channel.gd")

const Statistics = preload("statistics.gd")

const STATE = {
	CONNECT = 1,
	AWAITING_APPROVAL = 2,
	APPROVED = 8,
	CONNECTING = 3,
	CONNECTED = 4,
	DISCONNECT  = 5,
	DISCONNECTING = 6,
	DISCONNECTED = 7
}
# DEBUG helper
func debug (state):
	if (state == STATE.CONNECT):
		print("CONNECT")
	elif (state == STATE.AWAITING_APPROVAL):
		print("AWAITING_APPROVAL")
	elif (state == STATE.CONNECTED):
		print("CONNECTED")
	elif (state == STATE.APPROVED):
		print("APPROVED")
	elif (state == STATE.CONNECTING):
		print("CONNECTING")
	elif (state == STATE.DISCONNECTED):
		print("DISCONNECTED")
	else:
		print("State: ", str(state))

# Network peer
var peer

# Connection state
var state

var name
var ip
var port

var hail
var response
var disconnect_reason

var timeout_deadline
var last_handshake_time = 0
var sent_ping_time = 0
var sent_ping_nr = 0

var receive_channels = {}
var send_channels = {}

var avg_rtt
var time_offset

var statistics

# Signals
signal packet_sent
signal packet_received
signal packet_lost
signal packet_error

signal connecting
signal connected
signal disconnected

# Constructor
func _init(peer, ip = "localhost", port = 31415):
	self.peer = peer
	self.ip = ip
	self.port = int(port)
	self.name = "default name"
	self.time_offset = 0.0
	self.avg_rtt = -1.0
	self.sent_ping_time = 0
	self.sent_ping_nr = 0
	self.state = STATE.DISCONNECTED
	self.statistics = Statistics.new()

# Reset connection
func reset():
	name = ""
	_set_state(STATE.DISCONNECTED)

func _set_state(state):
	print("From:")
	debug(self.state)
	print("To:")
	debug(state)
	self.state = state

# Handshake called from network thread in peer when unconnected
func handshake_heartbeat(now):
	if (state == STATE.DISCONNECT):
		_shutdown("Handshake")
	elif (state == STATE.CONNECT):
		timeout_deadline = now + peer.config.connection.timeout_connect * 1000
		_set_state(STATE.CONNECTING)
	elif (now > timeout_deadline):
		_shutdown("Connect timeout", false)
	elif (now - last_handshake_time > 2000):
		if (state == STATE.CONNECTING):
			_send_connect(now, hail)
		elif (state == STATE.AWAITING_APPROVAL):
			last_handshake_time = now
		elif (state == STATE.APPROVED):
			_send_connect_response(now, response)
			_set_state(STATE.CONNECTED)
		elif (state == STATE.DISCONNECTED):
			print("Handshake called on disconnected connection")

# Heartbeat called fram network thread in peer when connected
func heartbeat(now, frames):
	if ((frames % peer.config.connection.infrequent_frame_interval) == 0):
		if (now > timeout_deadline):
			_shutdown("Connection timed out", false)
			return
				# Check if disconnect was requested
		elif (state == STATE.DISCONNECT):
			_shutdown(disconnect_reason)
			return
		# Send ping when connected
		elif (state == STATE.CONNECTED):
			if (now > sent_ping_time + peer.config.connection.ping_interval):
				_send_ping()

	if ((frames % peer.config.connection.frequent_frame_interval) == 0):
		# Lidgren does send ack and parse incoming acks here
		pass
		# for channel in send_channels:
		#	send_channels[channel].heartbeat(now)
	
	# Send queued
	for nr in send_channels:
		var channel = send_channels[nr]
		channel.send_queued_messages(now)

# Received library message while unconnected (called from network thread)
func received_handshake(now, channel, data):
	var message = data[1]
	
	if (channel == peer.Channel.connect):
		print(name, " received handshake CONNECT: ")
		if (state == STATE.DISCONNECTED):
			_set_state(STATE.AWAITING_APPROVAL)
			emit_signal("connecting", self, message)
		timeout_deadline = now + peer.config.connection.timeout_connect * 1000
		
	elif (channel == peer.Channel.connect_response):
		name = data[2]
		print("Received CONNECT RESPONSE from ", name)
		_set_state(STATE.CONNECTED)
		timeout_deadline = now + peer.config.connection.timeout_disconnect * 1000
		emit_signal("connected", self, message)

# Received library message while connected (called from network thread)
func received_library_message(now, channel, data):
	# PING
	if (channel == peer.Channel.ping):
		_received_ping(now, data[1])
	# PONG
	elif(channel == peer.Channel.pong):
		_received_pong(now, data[1], data[2])
	# CONNECT
	elif(channel == peer.Channel.connect):
		print("Connect response must have been lost - resending")
		_send_connect_response(now, response)
	# DISCONNECT
	elif(channel == peer.Channel.disconnect):
		print("Connection sent disconnect")
		_shutdown(data[1], false)
	else:
		print("Received unhandled library message on channel ", channel, ": ", str(data))

# Received application message (called from network thread)
func received_message(now, channel, data):
	var receiver
	if (! receive_channels.has(channel)):
		receiver = _create_receiver_channel(channel)
		receive_channels[channel] = receiver
	else:
		receiver = receive_channels[channel]
	receiver.receive_message(data)

# Create receiver channel based on type of channel (e.g. reliable, unreliable)
func _create_receiver_channel(channel):
	if (channel < peer.Channel.app_reliable):
		# Unreliable
		return UnreliableReceiveChannel.new(self, channel)
	else:
		return ReliableReceiveChannel.new(self, channel, peer.config)

func _received_ping(now, nr):
	_send_pong(nr)

func _received_pong(now, nr, remote_time):
	if (sent_ping_nr != nr):
		return
	var rtt = now - sent_ping_time
	var diff = (remote_time + (rtt / 2.0)) - now
	timeout_deadline = now + peer.config.connection.timeout_disconnect * 1000
	
	if (avg_rtt < 0):
		time_offset = diff
		avg_rtt = rtt
	else:
		avg_rtt = avg_rtt * 0.7 + rtt * 0.3
		time_offset = ((time_offset * (sent_ping_nr - 1)) + diff) / sent_ping_nr

# Send connect message (from clients)
func _send_connect(now, hail):
	print("Send connect")
	last_handshake_time = now
	var msg = [peer.Channel.connect, hail]
	_send(msg)

# Send connection response (from server)
func _send_connect_response(now, response):
	print("Send connect response")
	last_handshake_time = now
	var msg = [peer.Channel.connect_response, response, peer.config.server.name]
	_send(msg)

func _send_disconnect(reason):
	print("Send disconnect")
	var msg = [peer.Channel.disconnect, reason]
	_send(msg)

# Send ping to this connection
func _send_ping():
	sent_ping_time = OS.get_ticks_msec()
	sent_ping_nr += 1
	var msg = [peer.Channel.ping, sent_ping_nr]
	_send(msg)

# Send ping response to this connection
func _send_pong(ping_nr):
	var msg = [peer.Channel.pong, ping_nr, OS.get_ticks_msec()]
	_send(msg)

# Send message immediatly to this connection embedding channel correctly
func _send(msg):
	peer.socket.send(ip, port, msg)
	statistics.sent_packets += 1

# Shutdown this connection
func _shutdown(reason, notify = true):
	print ("Connection shut down: ", reason)
	
	for channel in send_channels:
		send_channels[channel].reset()
	
	if (notify):
		_send_disconnect(reason)
	
	_set_state(STATE.DISCONNECTED)
	emit_signal("disconnected", self, reason)

################################################################################
# External functions called not in network thread
################################################################################

func approve(response):
	self.response = response
	_set_state(STATE.APPROVED)

func disapprove(response):
	_shutdown(response)

# Called
func enqueue(msg, channel):
	if (! is_connected()):
		print("Cannot enqueue msg on disconnected connection")
		return
	var ch
	if (send_channels.has(channel)):
		ch = send_channels[channel]
	else:
		if (channel < peer.Channel.app_reliable):
			ch = UnreliableSendChannel.new(self, channel)
		else:
			ch = ReliableSendChannel.new(self, peer.config)
		send_channels[channel] = ch
	ch.enqueue(msg)

# Connect to this connection
func connect_to(hail):
	self.hail = hail
	_set_state(STATE.CONNECT)

# Disconnect because of reason
func disconnect_from(reason):
	_set_state(STATE.DISCONNECT)
	disconnect_reason = reason

# Send message immediatly to this connection
func send(msg):
	peer.socket.send(ip, port, msg)
	statistics.sent_packets += 1

# Queue message to this connection 
func send_message(msg, channel):
	peer.send_message(msg, self, channel)

# Connected?
func is_connected():
	return state == STATE.CONNECTED

func is_disconnected():
	return state == STATE.DISCONNECTED

# Connecting?
func is_connecting():
	return state == STATE.CONNECTING

func is_awaiting_approval():
	return state == STATE.AWAITING_APPROVAL
