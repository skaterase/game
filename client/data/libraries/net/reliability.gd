
extends Reference


var local_seq = 1 # sequence number to partner
var remote_seq = 0# most recent ack from partner

var received_queue = [] # for determining ack bits on outgoing packets
var pending_ack_queue = [] # for sent but not yet acked packets (packets kept for RTT_MAXIMUM * 2)

var config
var connection

signal packet_acked

func _init(connection, config):
	self.config = config
	self.connection = connection

# Send data reliably
func send(now, data):
	var seq = local_seq
	var ack = remote_seq
	var ack_bits = _generate_ack_bits()
	var packet = [seq, ack, ack_bits, data]
	
	connection.enqueue(packet)
	
	# Add sequence to pending ack queue
	pending_ack_queue.push_back({
		seq = seq, # sequence number
		lastSent = now,  # time in queue
		data = data,
		retries = 0
	})
	
	# Increment local sequence number
	local_seq += 1
	
	# Wrap sequence number when the signed integer turns negative
	if (local_seq < 0):
		local_seq = 1


# Receive packet from this connection
func receive(data):
	# Unwrap header and content
	var seq = data[0]
	var ack = data[1]
	var ack_bits = data[2]
	var content = data[3]
	
	# Add sequence to received queue
	received_queue.push_back(seq)
	
	# Update most recent sequence
	if (_is_seq_more_recent(seq, remote_seq)):
		remote_seq = seq
	
	# Check pending acks againts ack and ack_bits
	_process_ack(ack, ack_bits)
	
	# Signal packet received
	emit_signal("packet_received", self, content)

# Update connection
func update(now):
	# Update pending ack queue
	update_pending_ack_queue(now)
	# Update received queue
	update_received_queue()

# Update pending ack queue
func update_pending_ack_queue(now):
	var remove = []
	var index = -1
	# Update pending ack queue
	for item in pending_ack_queue:
		index += 1
		# Increase time in queue
		var delta = now - item.time
		item.time = now
		# Remove from pending ack queue if time in queue above threshold
		if (delta > config.reliability.rtt_maximum * 2.0):
			# Queue for removal
			remove.append(index)
			
			# Signal packet lost
			emit_signal("packet_lost", self, item)
	# Cannot remove elements while looping over them so need to do it in it's own loop.
	# Index of elements decreases by 1 for every removal so must account for it.
	index = 0
	for idx in remove:
		pending_ack_queue.remove(idx - index)
		index += 1

# Update received queue
func update_received_queue():
	if (!received_queue.empty()):
		received_queue.sort()
		var latest_seq = received_queue[received_queue.size() - 1]
		var min_seq = 0
		
		if (latest_seq >= 34):
			min_seq = latest_seq  - 34
		else:
			min_seq = config.reliability.max_sequence - (34 - latest_seq)
		
		# Remove old sequence numbers from received queue
		# This works because elements are sorted (above)
		while (!received_queue.empty() && !_is_seq_more_recent(received_queue[0], min_seq)):
			received_queue.pop_front()

# Process received acks
func _process_ack(ack, ack_bits):
	var acked = [] # acked items
	
	var index = -1 # index in for loop
	for item in pending_ack_queue:
		index += 1
		var is_acked = false
		var seq = item.seq
		if (seq == ack):
			is_acked = true
		elif (!_is_seq_more_recent(seq, ack)):
			var bit_index = _bit_index_for_seq(seq, ack)
			if (bit_index <= 31):
				is_acked = true
		if (is_acked):
			acked.append(index)
		
	# Remove acked from pending ack queue
	index = 0
	for idx in acked:
		pending_ack_queue.remove(idx - index)
		index += 1
		emit_signal("packet_acked", self)

# Calculate bit index of sequence number in ack bitfield
func _bit_index_for_seq(seq, ack):
	if (seq > ack):
		return ack + (config.reliability.max_sequence - seq)
	else:
		return ack - seq - 1

# Generate ack bits based on received_queue
func _generate_ack_bits(ack, received_queue):
	var ack = remote_seq
	var ack_bits = 0
	
	for seq in received_queue:
		if (seq == ack || _is_seq_more_recent(seq, ack)):
			break
		var bit_index = _bit_index_for_seq(seq, ack)
		if (bit_index <= 31):
			ack_bits |= 1 << bit_index
	
	return ack_bits

# Check if sequence number s1 is more recent than s2 while accounting for sequence number wrap around
func _is_seq_more_recent(s1, s2):
	return (s1 > s2) && (s1 - s2 <= config.reliability.seq_wrap_dist) || (s2 > s1) && (s2 - s1 > config.reliability.seq_wrap_dist)
