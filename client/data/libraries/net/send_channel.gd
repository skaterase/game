
extends "channel.gd"

const Queue = preload("queue.gd")

var send_queue

func _init(connection, channel).(connection, channel):
	self.send_queue = Queue.new()
	print("Send channel: ", str(channel), " Connection: ", connection.name, " ")

func enqueue(message):
	pass

func send_queued_messages(now):
	pass

func reset():
	send_queue.clear()
