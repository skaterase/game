
extends Reference

# Thread safe queue
# Inspired by https://github.com/lidgren/lidgren-network-gen3/blob/master/Lidgren.Network/NetQueue.cs

const DEFAULT_CAPACITY = 128

var items = []
var head
var size
var capacity

# Concurrency lock (mutex)
var _queue

func _init(initial_capacity = DEFAULT_CAPACITY):
	_queue = Mutex.new()
	capacity = initial_capacity
	items = []
	items.resize(capacity)
	head = 0
	size = 0

# Get size
func size():
	_queue.lock()
	var size = self.size
	_queue.unlock()
	return size

# Add item to end of queue
func enqueue(item):
	_queue.lock()
	if (size == capacity):
		capacity += 8
		items.resize(capacity)
	var index = (head + size) % capacity
	items.insert(index, item)
	size += 1
	_queue.unlock()

# Add items to end of queue
func enqueue_items(items):
	_queue.lock()
	if (items.size() == 0):
		return
	if (items.size() > capacity - size):
		capacity = capacity + items.size() + 8
		self.items.resize(capacity)
	for item in items:
		var index = (head + size) % capacity
		self.items.insert(index, item)
		size += 1
	_queue.unlock()

# Remove the first item in the queue
func dequeue():
	if (size == 0):
		return null
	_queue.lock()
	var item = items[head]
	items[head] = null
	head = (head + 1) % items.size()
	size -= 1
	_queue.unlock()
	return item

# Return 
func drain():
	if (size == 0):
		return []
	_queue.lock()
	var count = size
	var items_copy = range(0, size)
	for index in range(0, size):
		items_copy[index] = items[(index + head) % size]
	head = 0
	size = 0
	_queue.unlock()
	return items_copy

func clone():
	print("Queue clone")
	var clone = []
	_queue.lock()
	for item in items:
		clone.append(item)
	_queue.unlock()
	return clone

func clear():
	_queue.lock()
	items.clear()
	head = 0
	size = 0
	_queue.unlock()
