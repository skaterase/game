# -----------------------------------------------------------------------------
# Server class
#
# Inheritance: Reference > peer.gd > server.gd
# -----------------------------------------------------------------------------
extends "peer.gd"

# Constructor
func _init(config).(config):
	# Set listening port from config
	port = config.server.listening_port
	name = "server"

# -----------------------------------------------------------------------------
# Server interface
# ------------------------------------------------------------------------------

# Broadcast data to everyone connected to this peer
func broadcast(msg, channel_nr = MessageType.app_unreliable):
	lock_connections()
	for connection in connections:
		send_message(msg, connection, channel_nr)
	unlock_connections()
