extends Reference

# Network protocol identifier
const PROTOCOL_ID = 0xf0e00000

# UDP socket
var socket

func _init():
	socket = PacketPeerUDP.new()

# Listen on port
func open(port):
	if (socket.listen(port) == OK):
		return true
	else:
		print("Failed to listen on port ", port)
		return false
	
# Stop listening
func close():
	if (socket.is_listening()):
		socket.close()

# Poll for packets for up to ms milliseconds
func poll(ms):
	var count = 0
	var nr_of_packets_available = socket.get_available_packet_count()
	while (nr_of_packets_available < 1):
		OS.delay_usec(100)
		nr_of_packets_available = socket.get_available_packet_count()
		count += 1
		if (count >= 10 * ms):
			break
	return nr_of_packets_available

# Get next packet from socket
func next_packet():
	# Make sure socket has available packets (by calling socket.get_available_packet_count)
	# before calling this method
	
	# NOTE:
	# 	socket.get_var() must be called before socket.get_packet_ip() and
	# 	socket.get_packet_port() or else ip will be 0.0.0.0 and port will be 0
	var data = socket.get_var()
	var error = socket.get_packet_error()
	var ip = socket.get_packet_ip()
	var port = socket.get_packet_port()
	
	# Detect packet errors
	if (error != OK):
		data = "Low level error"
	elif (typeof(data) != TYPE_ARRAY):
		error = !OK
		data = "Packet type is not Array"
	elif ((data[0] & 0xfff00000) != PROTOCOL_ID):
		error = !OK
		data = "Unknown protocol identifier: " + str(data[0] & 0xfff00000)
	
	# Print error
	if (error != OK):
		print("ERROR: ", data)
	else:
		# Strip protocol identifier
		data[0] &= 0xfffff
	
	# Return packet
	return {
		ip = ip,
		port = port,
		data = data,
		error = error
	}

# Send data to ip on port using socket
func send(ip, port, data):
	socket.set_send_address(ip, port)
	data[0] += PROTOCOL_ID # embed protocol id
	var error = socket.put_var(data)
	if (error != OK):
		print("ERROR: socket.put_var(data) !OK: ", str(error))
	return error
