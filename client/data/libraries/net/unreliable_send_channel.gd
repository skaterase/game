
extends "send_channel.gd"

func _init(connection, channel).(connection, channel):
	pass

func enqueue(message):
	message.push_front(channel)
	send_queue.enqueue(message)

func send_queued_messages(now):
	var messages = send_queue.drain()
	if (messages.empty()):
		return
	for msg in messages:
		if (msg == null):
			print("msg is null")
			continue
		connection.send(msg)
