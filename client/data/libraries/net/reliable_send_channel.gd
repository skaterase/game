
extends "send_channel.gd"

const Reliability = preload("reliability.gd")

var reliability setget _set_reliability

func _init(connection, channel, config).(connection, channel):
	reliability = Reliability.new(connection, config)


func _set_reliability(_reliability):
	reliability = _reliability
	reliability.connect("packet_lost", self, "_on_packet_lost")

func enqueue(message):
	message.push_front(channel)
	send_queue.enqueue(message)

# Called from connection heartbeat
func send_queued_messages(now):
	# Resend unacked
	# for unacked in reliability.pending_ack_queue:
	# 	connection.statistics.resent_packets += 1
	
	# Queued send
	for i in range(0, send_queue.size()):
		var msg = send_queue.dequeue()
		reliability.send(now, msg)
