
extends "receive_channel.gd"

const Reliability = preload("reliability.gd")

var reliability

func _init(connection, channel, config).(connection, channel):
	reliability = Reliability.new(connection, config)

func receive_message(msg):
	reliability.receive(msg)
	
	# connection.peer.enqueue(msg)
