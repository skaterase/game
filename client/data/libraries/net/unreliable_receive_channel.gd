
extends "receive_channel.gd"

func _init(connection, channel).(connection, channel):
	pass

func receive_message(msg):
	connection.emit_signal("packet_received", connection, channel, msg)
