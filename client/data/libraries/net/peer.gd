# -----------------------------------------------------------------------------
# Base class for network client and network server
#
# Inherited by: client.gd, server.gd
# -----------------------------------------------------------------------------
extends Reference

const Connection = preload("connection.gd")
const Socket = preload("socket.gd")
const Queue = preload("queue.gd")
const Statistics = preload("statistics.gd")

var config
var name
var socket
var port

# List of connections
var connection_lookup = {}
var connections = []
var connections_lock = Mutex.new()
var handshakes = {}

func get_connections():
	lock_connections()
	var copy = range(0, connections.size())
	for index in range(0, copy.size()):
		copy[index] = connections[index]
	unlock_connections()
	return copy

func lock_connections(who = null):
	if (who != null):
		print("Locked by: ", who)
	connections_lock.lock()

func unlock_connections(who = null):
	if (who != null):
		print("Unlocked by: ", who)
	connections_lock.unlock()

var is_running

var network_thread

var incoming_queue = Queue.new()
var outgoing_queue = Queue.new()

var last_heartbeat

var frames

var statistics

var shutdown_reason

var Channel = {
	connect = 1,
	connect_response = 2,
	disconnect = 3,
	ping = 4,
	pong = 5,
	
	app_unreliable = 10,
	app_reliable = 50
}

signal start # peer started
signal stop
signal connected # connection connected
signal accepted # connection accepted
signal disconnected # connection disconnected

# Constructor
func _init(config):
	self.config = config
	self.statistics = Statistics.new()
	self.is_running = false
	self.frames = 0
	self.last_heartbeat = 0.0
	self.socket = Socket.new()

# Listen on port, start idle processing and start network thread
func start():
	if (is_running == true):
		print("Start called on already running peer, ignoring")
		return false
	
	if (port == 0):
		print("Port is not configured")
		return false
	
	# Open socket
	if (socket.open(port)):
		is_running = true
		# Start network thread
		network_thread = Thread.new()
		network_thread.start(self, "_network_thread")
		# Wait for network thread to start
		OS.delay_msec(50)
		return true
	else:
		print("Failed to open port ", port)
		return false

# Network thread
func _network_thread(userdata):
	while (is_running):
		_heartbeat();
	_shutdown(shutdown_reason)

# Network thread heartbeat
func _heartbeat():
	var now = OS.get_ticks_msec()
	var delta = now - last_heartbeat
	
	# Max connection heartbeats per second
	var max_chb_per_sec = 1000 - connections.size()
	
	if (delta > 1.0 / max_chb_per_sec || delta < 0.0):
		frames += 1
		last_heartbeat = now
		
		# Connection handshake heartbeat
		if ((frames % 3) == 0):
			lock_connections()
			for key in handshakes.keys():
				var connection = handshakes[key]
				connection.handshake_heartbeat(now)
				if connection.is_disconnected():
					handshakes.erase(key)
				if (connection.is_connected()):
					_accept_connection(connection)
			unlock_connections()
		
		# Connection heartbeat
		lock_connections()
		for uid in connection_lookup.keys():
			var connection = connections[connection_lookup[uid]]
			connection.heartbeat(now, frames)
			if (connection.is_disconnected()):
				_remove_connection(connection)
		unlock_connections()
	
	# Wait up to 1 ms for packets to arrive
	var packet_count = socket.poll(1)
	if (packet_count < 1):
		# No packets arrived
		return
	
	now = OS.get_ticks_msec()
	
	# Receive packets from socket
	while (packet_count > 0):
		packet_count -= 1
		var packet = socket.next_packet()
		
		if (packet.error != OK):
			continue
		
		var sender = null
		var uid = packet.ip + str(packet.port)
		if (connection_lookup.has(uid)):
			sender = connections[connection_lookup[uid]]
		var channel = (packet.data[0]) & 0xff
		
		# Types below app_unreliable are resereved for library internal use (e.g.: ping, pong, connect, disconnect, etc..)
		if (channel < Channel.app_unreliable):
			if (sender != null):
				sender.received_library_message(now, channel, packet.data)
			else:
				_received_library_message(now, channel, packet)
		else:
			if (sender == null):
				# Ignore application message from unconnected source
				continue
			else:
				sender.received_message(now, channel, packet.data)
		statistics.received_packets += 1

func _shutdown(reason):
	var list = []
	lock_connections()
	for connection in connections:
		connection.disconnect_from(reason)
	for index in handshakes:
		var connection = handshakes[index]
		connection.disconnect_from(reason)
	unlock_connections()
	
	_heartbeat()
	
	# Close socket
	socket.close()
	
	# Wait for network thread to finish
	network_thread.wait_to_finish()
	
	# Signal stop
	emit_signal("stop", "Shutdown")

func _received_library_message(now, channel, packet):
	var uid = packet.ip + str(packet.port)
	if (channel == Channel.connect):
		if (! handshakes.has(uid)):
			handshakes[uid] = _create_connection(packet.ip, packet.port)
		handshakes[uid].received_handshake(now, channel, packet.data)
	elif (channel == Channel.connect_response):
		if (! handshakes.has(uid)):
			print("Received connect response but connection not in handshakes")
		else:
			handshakes[uid].received_handshake(now, channel, packet.data)
	elif (channel == Channel.disconnect):
		print("Received disconnect from unconnected connection")
	else:
		print("Received unknown message type ", channel)

# Create new connection
func _create_connection(ip, port):
	# The connection uses this host (self) to send packets
	var connection = Connection.new(self, ip, port)
	
	# Connect signals handlers:
	
	# Packet events
#	connection.connect("packet_sent", self, "_on_packet_sent")
	connection.connect("packet_received", self, "_on_packet_received")
#	connection.connect("packet_acked", self, "_on_packet_acked")
#	connection.connect("packet_lost", self, "_on_packet_lost")
#	connection.connect("packet_error", self, "_on_packet_error")
#	
#	# Connection state events
	connection.connect("disconnected", self, "_on_disconnected")
#	connection.connect("listening", self, "_on_listening")
	connection.connect("connecting", self, "_on_connecting")
	connection.connect("connected", self, "_on_connected")
#	connection.connect("idled", self, "_on_idled")
	
	return connection

# Add specified connection to connection list
func _add_connection(connection):
	print("Adding connection: ", connection.name)
	var uid = connection.ip + str(connection.port)
	if (handshakes.has(uid)):
			handshakes.erase(uid)
	if (connection_lookup.has(uid)):
		print("Add connection already in list")
		return null
	else:
		connections.push_back(connection)
		connection_lookup[uid] = connections.size() - 1
		return uid
	return uid

# Remove specified connection from connection list
func _remove_connection(connection):
	var id = connection.ip + str(connection.port)
	if (connection_lookup.has(id)):
		var index = connection_lookup[id]
		connections.remove(index)
		connection_lookup.erase(id)
		for key in connection_lookup:
			if (connection_lookup[key] >= index):
				connection_lookup[key] -= 1
	else:
		print("Remove connection but not in list")

func _accept_connection(connection):
	if (_add_connection(connection) == null):
		print("Connection already in list")
	else:
		emit_signal("accepted", connection)

# Connect
func _connect_to(ip, port, hail = null):
	if (ip == "localhost"):
		ip = "127.0.0.1"
	var uid = ip + str(port)
	print(name, " connecting to ", ip, " ", port, " with ", hail)
	if (! is_running):
		print("Can not connect when not running. Call start first")
		return
	
	if (connection_lookup.has(uid)):
		print("Already connected to ", ip, " on ", port)
		return
	
	if (handshakes.has(uid)):
		print("Connect called but connection already present in list of handshaking connections")
	else:
		lock_connections("Connect to")
		handshakes[uid] = _create_connection(ip, port)
		unlock_connections("Connect to")
		handshakes[uid].connect_to(hail)

# Stop
func stop(reason = null):
	if (is_running == false):
		print("Stop called when not running")
		return
	else:
		print ("Stopping ", name)
	shutdown_reason = reason
	is_running = false

# Send message from peer to connection
func send_message(msg, connection, channel = Channel.app_unreliable):
	if (connection == null):
		return
	lock_connections()
	connection.enqueue(msg, channel)
	unlock_connections()

func read_message():
	return incoming_queue.dequeue()

func read_messages():
	return incoming_queue.drain()

# -----------------------------------------------------------------------------
# Signal handlers for connections
#
# Overridde in inherited class
# -----------------------------------------------------------------------------

# Handle connection connected
func _on_connected(connection, hail):
	print("On connected ", connection.name, ": ", hail)
	_accept_connection(connection)
	emit_signal("connected", connection, hail)

# Handle connection disconnected
func _on_disconnected(connection, reason):
	print("On disconnected ", connection.name, ": ", reason)
	emit_signal("disconnected", connection, reason)

func _on_connecting(connection, message):
	print("On connecting: ", message)
	emit_signal("connected", connection, message)

# Handle packet sent
func _on_packet_sent(connection, packet):
	pass

# Handle packet was acked
func _on_packet_acked(connection):
	pass

# Handle received packet
func _on_packet_received(connection, channel, message):
	incoming_queue.enqueue([connection, channel, message])

# Handle lost packet
func _on_packet_lost(connection, item):
	pass

# Handle resent packet
func _on_packet_resent(connection, item):
	pass

# Handle packet error
func _on_packet_error(error, data):
	pass
