extends Reference

# File Paths
const SYSTEM_CONFIG_FILE = "res://configuration.cfg"
const USER_CONFIG_FILE = "user://configuration.cfg"

const reliability = {
	rtt_maximum = 1.0, # maximum expected rtt
	rtt_alpha = 0.1, # rtt exponential smoothing value
	max_sequence = 0x7fffffff, # maximum sequence number (largest positive 32 bit signed int)
	seq_wrap_dist = 0x1000 # 4096 (136.53 seconds of 30 packets per second)
}
const flow_control = {
	rtt_threshold = 250.0, # threshold time (ms) for dropping to bad mode when good
	server_good_rate = 20,
	server_bad_rate = 10,
	client_good_rate = 30,
	client_bad_rate = 10,
	penalty_time = 4.0
}
const server = {
	name = "Group 7 server",
	listening_port = 31415,
	maxclients = 32, # not used yet
	private = false # not used yet
}
const client = {
	listening_port = 31416,
	server_ip = "localhost",
	server_port = 31415,
}
const connection = {
	ping_interval = 3000,
	timeout_connect = 10.0,
	timeout_disconnect = 10.0,
	timeout_idle = 5.0,
	resend_handshake_interval = 3000,
	infrequent_frame_interval = 8,
	frequent_frame_interval = 3
}
const default_config = {
	reliability = reliability,
	flow_control = flow_control,
	server = server,
	client = client,
	connection = connection
}

# Load configuration from configuration files
# User configuration is preferred over system configuration
# Default configuration values are used if corresponding values are not present in any of the config files
func load_config():
	var configuration = default_config
	
	var systemConfigFile = ConfigFile.new()
	# If system configuration file is not present we generate it
	if (systemConfigFile.load(SYSTEM_CONFIG_FILE) != OK):
		save_config(default_config, SYSTEM_CONFIG_FILE)
	
	# If user configuration file is not present we generate it with key/value pairs commented out
	var userConfigFile = ConfigFile.new()
	if (userConfigFile.load(USER_CONFIG_FILE) != OK):
		save_config(default_config, USER_CONFIG_FILE, true)
	
	for configFile in [ systemConfigFile, userConfigFile ]:
		# We only process section and keys present in default configuration
		for section in configuration:
			if (configFile.has_section(section)):
				for key in configuration[section]:
					if (configFile.has_section_key(section, key)):
						var default_value = configuration[section][key]
						var value = configFile.get_value(section, key, default_value)
						# Sanity check
						if (typeof(value) != typeof(default_value)):
							value = default_value
						configuration[section][key] = value
	
	return configuration

# Save configuration to file using default_configuration as default
func save_config(configuration, path, commented = false):
	# Initiate a new ConfigFile every time you load a file
	var configFile = ConfigFile.new()
	
	# Add each key and value to section in configuration file
	for section in configuration:
		for key in configuration[section]:
			var value = configuration[section][key]
			if (commented):
				key = "; " + key
			configFile.set_value(section, key, value)
	
	# Save file
	configFile.save(path)

# Checks if a file/resource exists in the path given
# returns a boolean
func file_exists(path):
	return File.new().file_exists(path)
