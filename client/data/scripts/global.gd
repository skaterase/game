extends Node

# Load FileLoader
const FileLoader = preload("file_loader.gd")

# Network server class
const GameServer = preload("res://data/net/scripts/game_server.gd")

# Network client class
const GameClient = preload("res://data/net/scripts/game_client.gd") # Client class

var config setget ,_get_config

func get_current_scene():
	var root = get_tree().get_root()
	return root.get_child(root.get_child_count() - 1)

func goto_main_scene():
	if (! get_current_scene().has_node("root_node")):
		get_tree().change_scene("res://data/main/main.tscn")

func goto_main_menu():
	if (! get_current_scene().has_node("node")):
		get_tree().change_scene("res://data/main_menu/main_menu.tscn")

func play_solo():
	goto_main_scene()
	call_deferred("_spawn_offline_player")

func _spawn_offline_player():
	get_current_scene().get_node("world").spawn_offline_player("Offline player")

func host_server():
	goto_main_scene()
	var server = GameServer.new(config)
	call_deferred("_add_to_main", server)

func connect_to_server(ip, port):
	config.client.server_ip = ip
	if (int(port) != null):
		config.client.server_port = port
	var client = GameClient.new(config)
	goto_main_scene()
	call_deferred("_add_to_main", client)

func _add_to_main(peer):
	var current_scene = get_current_scene()
	peer.set_name("net")
	current_scene.add_child(peer)

func _on_net_start(message):
	print("Global: On net start")

func _on_net_stop(reason):
	goto_main_menu()

func _get_config():
	if (config == null):
		config = FileLoader.new().load_config()
	return config
