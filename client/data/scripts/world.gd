
extends Node2D

# Player scene
const Player = preload("res://data/player/player.tscn")

# Different types of player scripts
const PLAYER = {
	OFFLINE = "res://data/player/scripts/player_offline.gd",
	SERVER = "res://data/player/scripts/player_server.gd",
	OTHER = "res://data/player/scripts/player_other.gd",
	ONLINE = "res://data/player/scripts/player.gd"
}


# Handle quit request
func _notification(what):
	if (what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST):
		print("World quit")
		reset()

# Return the coordinates in the center of the worlds viewport
func center():
	var width = get_tree().get_root().get_rect().size.width
	var heigth = get_tree().get_root().get_rect().size.height
	return Vector2(width / 2.0, heigth / 2.0)

func spawn_player(script, node_name):
	var node = Player.instance()
	node.set_script(load(script))
	node.set_name(node_name)
	node.set_pos(center())
	node.add_to_group("players")
	add_child(node)
	return node

# Spawn own player on client
func spawn_self(name):
	print("Spawn self: ", name)
	return spawn_player(PLAYER.ONLINE, name)

# Spawn other player on client
func spawn_other_player(name):
	print("Spawn other player: ", name)
	return spawn_player(PLAYER.OTHER, name)

# Spawn player on server
func spawn_server_player(name):
	print("Spawn server player: ", name)
	return spawn_player(PLAYER.SERVER, name)

# Spawn offline player (for testing without network)
func spawn_offline_player(name):
	print("Spawning offline player: ", name)
	var node = spawn_player(PLAYER.OFFLINE, name)
	# Remove offline player from group cause server
	# send updates about players in this group
	node.remove_from_group("players")
	return node

# Destroy a node
func destroy(name):
	if (name != null && has_node(name)):
		print("Destroy player: ", str(name))
		var node = get_node(name)
		node.queue_free()
	else:
		print("Can't destroy player because it does not exist")

# Reset world by removing all nodes in group players
func reset():
	for player in get_tree().get_nodes_in_group("players"):
		player.queue_free()
