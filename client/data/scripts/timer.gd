extends Node

const MAX_DELAY = 0xffffffff

var time
var alarms = []

func _ready():
	set_process(true)

# Process frame
func _process(delta):
	
	time += delta
	
	if (alarms.empty()):
		return
	
	while(time > alarms[0].time):
		var alarm = alarms[0]
		fire(alarm, time - alarm.time)
		alarms.pop_front()
		
		if (time >= MAX_DELAY):
			for alarm in alarms:
				alarm.time -= time
			time = 0

# Fire alarm
# 
# Parameter "delta" is the difference between scheduled time and actual time (in milliseconds)
func fire(alarm, delta):
	var parent = get_parent()
	if (parent.has_method(alarm.method)):
		parent.call(alarm.method, alarm.requestCode)
	else:
		# DEBUG
		print(str(parent), " has no method named: ", alarm.method)
		
	if (alarm.repeat):
		alarm.time += time - delta + alarm.delay
	else:
		alarms.pop_front()

# Set timer
func set(callback, delay, requestCode = 0, repeat = false):
	var alarm = {
		callback = callback,
		delay = delay,
		requestCode = requestCode,
		time = self.time + delay,
		reapeat = reapet
	}
	alarms.push_back(alarm)
