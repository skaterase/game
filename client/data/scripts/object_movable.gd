 # Hierarchy: object_movable.gd => RigidBody2D
extends RigidBody2D

onready var speed = 1000.0 # Movement speed
var force = Vector2(0, 0) # Movement force

# Forces
const FORCE = {
	ZERO = Vector2(0, 0),
	UP = Vector2(0, -1),
	DOWN = Vector2(0, 1),
	LEFT = Vector2(-1, 0),
	RIGHT = Vector2(1, 0)
}

# Running processes
func _integrate_forces(state):
	# Apply force
	apply_force(state)
	
	# Get directional vector
	force = force.normalized()
	
	# Move object
	state.set_linear_velocity(state.get_linear_velocity() + force * speed * state.get_step())

# Used to overwrite force and direction
func apply_force(state):
	pass
