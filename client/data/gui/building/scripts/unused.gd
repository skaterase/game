extends Node2D


# Constants
const CELL_SIZE = Vector2(32, 32) # The default tilesize (tiles can occupy multiple tiles; but not less than this)
const NO_COLLISION = 4 # Does not collide with anything

# export(PackedScene) var tileset_state # tileset state is added from editor

# Tilemaps
onready var tilemap_ground = get_node("tilemap_ground") # GROUND
onready var tilemap_wall = get_node("tilemap_wall") # WALL
onready var tilemap_info = get_node("tilemap_info") # WALL
var tilemap_ghost # Used to preview building placement without interfearing with actual block placement

# Previous tile position - Used to prevent looping when we have not moved tile position
var previous_tile_pos = Vector2(-1, -1)

# Hover on Tile
var hover_tile_pos
var hover_tile

# Misc
var is_building = true # false by default, true while working

func _ready():
	# Processes
	set_process(true)
	set_process_input(true)

	# Generate the ghost nodes - TODO: Later change this depending on selected type of block (ground, wall, etc.)
	tilemap_ghost = add_duplicate_node(tilemap_wall, NO_COLLISION) # TileSet node, int layer


func _process(delta):
	# Tile position the mouse is hovering over
	hover_tile_pos = tilemap_wall.world_to_map(get_local_mouse_pos())
	hover_tile = tilemap_wall.get_cellv(hover_tile_pos)

	# Preview block placement
	preview_block_placement()

	# Unfinished: Working on displaying grids around the player to make it easier to view block placement
	# draw_grid()






# Duplicates and adds clone to self
func get_duplicate_node(node, layer):
	var new_node = node.duplicate()
	new_node.set_collision_layer(layer)
	new_node.set_collision_mask(layer)
	self.add_child(new_node)
	return new_node



func _input(event):
	if(is_building):
		if (event.type == InputEvent.MOUSE_BUTTON):
			# Check if space is available
			if(can_build()):

				pass



func can_build():
	return hover_tile == -1 # returns true if it is empty

# Draws grid all over screen
func draw_grid():

	var window_x = int(OS.get_window_size().x)
	var window_y = int(OS.get_window_size().y)

	var grid_hori = window_x / CELL_SIZE.x / 3 # number of grids horizontal
	var grid_vert = window_y / CELL_SIZE.y / 3 # number of grids vertical

	var player_node = get_node("../player")
	var player_pos = player_node.get_pos()

	var tile_pos = tilemap_wall.world_to_map(player_pos)

	tilemap_info.get_cellv(player_pos)

	for x in range(tile_pos.x - grid_hori, grid_hori + tile_pos.x, 1):
		for y in range(tile_pos.y - grid_vert, grid_vert + tile_pos.y, 1):
			if(tilemap_info.get_cellv(Vector2(x, y)) != 0): # Prevent looping already placed grids
				tilemap_info.set_cellv(Vector2(x, y), 0)


# When this is running; will display the currently selected block
func preview_block_placement():

	# If the tile is empty
	if(hover_tile == -1):
		# We prevent looping if we have already done this
		if(previous_tile_pos != hover_tile_pos):

			# Clean previous tile
			tilemap_ghost.set_cellv(previous_tile_pos, -1)
			tilemap_info.set_cellv(previous_tile_pos, -1)

			pass

			# As long as the tile is empty
			if(hover_tile == -1):
				tilemap_ghost.set_cellv(hover_tile_pos, 0)
				tilemap_info.set_cellv(hover_tile_pos, 1)
				pass
			previous_tile_pos = hover_tile_pos
