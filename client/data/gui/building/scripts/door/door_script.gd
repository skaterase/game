
extends Area2D

var is_open = false


func _on_body_enter( body ):
	print("ENTERR")
	if (!is_open && body extends preload("res://data/player/scripts/player.gd")):
		is_open = true
		print("OOPEN DOOR!")

func _on_body_exit( body ):
	if (is_open && body extends preload("res://data/player/scripts/player.gd")):
		print("LEAVE")
		is_open = false
		print("CLOSE DOOR!")