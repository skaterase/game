extends Node2D

export var width = 30
export var height = 30
var array = []
var array_cube = []
export(int,0,100) var thresh

var thread = Thread.new()

func _ready():
	randomize()
	#setup
	for i in range(width*height):
		array.append(0)
		array_cube.append(null)
	#call_deferred("march")
	thread.start(self, "generate",null)
	#call_deferred("generate")
	
	

func generate(param):
	thresh = float(thresh*.01*.5)
	print(thresh)
	#rando walls
	for x in range(width):
		for y in range(height):
			set_array(x,y,round(rand_range(0,1)))
			if x == 0 or x == width-1 or y==0 or y==height-1:
				set_array(x,y,1)
			else:
				#threshold is in rand_range
				
				set_array(x,y,round(rand_range(0,.5+thresh)))
	for i in range(1):
		#first smooth
		for x in range(width):
			for y in range(height):
				if !(x == 0 or x == width-1 or y==0 or y==height-1):
					var count = 0
					for xx in range(3):
						for yy in range(3):
							var n = get_array(x-1+xx,y-1+yy)
							count+=n
					if count >4:
						set_array(x,y,1)
		# march()
		for i in range(2):
			#reverse smooth
			for x in range(width):
				for y in range(height):
					if !(x == 0 or x == width-1 or y==0 or y==height-1):
						var count = 0
						for xx in range(3):
							for yy in range(3):
								var n = get_array(x-1+xx,y-1+yy)
								count+=n
						if count <5:
							set_array(x,y,0)
			# march()
	call_deferred("march")
		

func march():
	#Marching Cubes
	for xx in range(width):
		for yy in range(height):
			var p = array_cube[xx+(width*yy)]
			if array_cube[xx+(width*yy)]!=null:
				array_cube[xx+(width*yy)].free()
			if !(xx == 0 or xx == width-1 or yy==0 or yy==height-1):
				var bin = 0
				bin+=get_array(xx,yy)
				bin+=get_array(xx+1,yy)*2
				bin+=get_array(xx,yy+1)*4
				bin+=get_array(xx+1,yy+1)*8
				#issues are here
				p = Polygon2D.new()
				var x = xx*16+8
				var y = yy*16+8
				if bin!=0:
					p.set_color(Color(0,0,0,.21))
					get_tree().get_root().add_child(p)
					
					
				if bin == 1:
					p.set_polygon([Vector2(x,y), Vector2(x+8,y),Vector2(x,y+8)])
				if bin == 2:
					p.set_polygon([Vector2(x+16,y), Vector2(x+16,y+8),Vector2(x+8,y)])
				if bin == 3:
					p.set_polygon([Vector2(x,y), Vector2(x+16,y),Vector2(x+16,y+8),Vector2(x,y+8)])
				if bin == 4:
					p.set_polygon([Vector2(x,y+8), Vector2(x+8,y+16),Vector2(x,y+16)])
				if bin == 5:
					p.set_polygon([Vector2(x,y), Vector2(x+8,y),Vector2(x+8,y+16),Vector2(x,y+16)])
				if bin == 6:
					p.set_polygon([	Vector2(x,y+16), Vector2(x,y+8),Vector2(x+8,y),
									Vector2(x+16,y), Vector2(x+16,y+8),Vector2(x+8,y+16)])
				if bin == 7:
					p.set_polygon([Vector2(x,y), Vector2(x+16,y),Vector2(x+16,y+8),Vector2(x+8,y+16),Vector2(x,y+16)])
				if bin == 8:
					p.set_polygon([Vector2(x+16,y+8), Vector2(x+16,y+16),Vector2(x+8,y+16)])
				if bin == 9:
					p.set_polygon([	Vector2(x,y), Vector2(x+8,y),Vector2(x+16,y+8),
									Vector2(x+16,y+16), Vector2(x+8,y+16),Vector2(x,y+8)])
				if bin == 10:
					p.set_polygon([Vector2(x+8,y), Vector2(x+16,y),Vector2(x+16,y+16),Vector2(x+8,y+16)])
				if bin == 11:
					p.set_polygon([Vector2(x,y+8), Vector2(x,y),Vector2(x+16,y),Vector2(x+16,y+16),Vector2(x+8,y+16)])
				if bin == 12:
					p.set_polygon([Vector2(x,y+8), Vector2(x+16,y+8),Vector2(x+16,y+16),Vector2(x,y+16)])
				if bin == 13:
					p.set_polygon([Vector2(x+8,y), Vector2(x+16,y+8),Vector2(x+16,y+16),Vector2(x,y+16),Vector2(x,y)])
				if bin == 14:
					p.set_polygon([Vector2(x+8,y), Vector2(x+16,y),Vector2(x+16,y+16),Vector2(x,y+16),Vector2(x,y+8)])
				if bin == 15:
					p.set_polygon([Vector2(x,y), Vector2(x+16,y),Vector2(x+16,y+16),Vector2(x,y+16)])
				if bin == 16:
					p.set_polygon([Vector2(x+8,y+16), Vector2(x+16,y+8),Vector2(x+16,y+16)])
			else:
				var p = Polygon2D.new()
				var x = xx*16+8
				var y = yy*16+8
				p.set_color(Color(0,0,0,.21))
				get_tree().get_root().add_child(p)
				p.set_polygon([Vector2(x,y), Vector2(x+16,y),Vector2(x+16,y+16),Vector2(x,y+16)])
			#array_cube[xx+(width*yy)]=p


#drawing - to be replaced with marching cubes
func _sdraw():
	for x in range(width):
		for y in range(height):
			var col
			if get_array(x,y) !=0:
				col = Color(1,1,1,.1)
			else:
				col = Color(0,0,0)
			draw_rect(Rect2(Vector2(16*x,16*y),Vector2(16,16)),col)

#1d array to 2d array stuff
func set_array(x,y,value):
	array[x+(width*y)]=value

func get_array(x,y):
	return array[x+(width*y)]