
extends Node2D

# Path to the Tilemaps
const TILEMAP_PATH = "tilemaps/"

# Tilemaps
onready var tilemap_floor = get_node(TILEMAP_PATH + "tilemap_floor") # floor (Everything the player can stand on)
onready var tilemap_wall = get_node(TILEMAP_PATH + "tilemap_wall") # WALL Level blocks (Walls, Doors, Windows, etc.)
onready var tilemap_info = get_node(TILEMAP_PATH + "tilemap_info") # INFO

# EDITOR SELECTIONS
onready var editor_selections = {"floor": tilemap_floor, "wall": tilemap_wall, "door": tilemap_wall}
onready var editor_selected = "wall" # Current selected editor option

# Editor GUI
onready var editor_gui = get_node("gui/editor")

# Placement Vars
var mouse_position # Current mouse position on the screen
var hovering_tile_position # Using mouse_position we get the Vector2 for the tile the mouse is on
var previous_tile_position # Used to prevent looping placement scripts in same place
var is_active = false # Whether or not building mode is active
var console_enabled = false # Used to prevent tile placement when true
var building_enabled = false # Used to prevent tile placement when true
var button_array = [] # Button node array for Building GUI

var holding_lmb = false # When Left Mouse Button is held down
var place_diagonal = false # Used to determine when we are placing diagonally
var place_rectangular # Used to determine when we are placing rectangular floors
var tile_array = Array() # Used to store certain tiles for various purposes in order to get previous coordinates
var direction = Vector2(0, 0) # A normal for which direction(s) we are currently placing in (diagonally)


func _ready():
	editor_gui.hide() 
	setup_connect()

# Handle quit request
func _notification(what):
	if (what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST):
		# Free resources
		tilemap_floor.queue_free()
		tilemap_info.queue_free()
		tilemap_wall.queue_free()


# Setup connections (singletons)
func setup_connect():
	# Singletons for buttons and GUI in building
	var panel_editor = find_node("panel_editor")
	panel_editor.connect("mouse_enter", self, "on_button_hovering", [ true ])
	panel_editor.connect("mouse_exit", self, "on_button_hovering", [ false ])
	
	button_array = find_node("grid_container").get_children()
	for button in button_array:
		# Hovering
		button.connect("mouse_enter", self, "on_button_hovering", [ true ])
		button.connect("mouse_exit", self, "on_button_hovering", [ false ])
		
		# Pressed
		button.connect("pressed", self, "selection_button_pressed", [ button, button.get_name() ])
	
	# Subscribe to notifications from console (enabled/disabled)
	get_node("../console").connect("console", self, "on_console")



# Button GUI pressed - Handles when Selection buttons have been pressed
func selection_button_pressed(button, name):
	if(!console_enabled):
		# Turn off disable on all buttons
		for button in button_array:
			button.disabled = false
		
		if(name == "floor"):
			editor_selected = "floor"
			button_array[0].disabled = true
			pass
		elif(name == "wall"):
			editor_selected = "wall"
			button_array[1].disabled = true
			pass
		elif(name == "door"):
			editor_selected = "door"
			button_array[2].disabled = true


func _input(event):
	if(building_enabled):
		# Get latest coordinates
		mouse_position = get_global_mouse_pos()
		hovering_tile_position = editor_selections[editor_selected].world_to_map(mouse_position)
		preview_block_placement()
	
		# Mouse Movement
		if(event.type == InputEvent.MOUSE_MOTION):
			# Rectangle Floor Placement
			if(holding_lmb && place_diagonal && editor_selected == "floor"):
				if(hovering_tile_position != previous_tile_position):
					place_rectangle_floor()
					previous_tile_position = hovering_tile_position
			
			# Diagonal Tile (Wall/Floor) Placement
			if(holding_lmb && place_rectangular && !place_diagonal):
				if(hovering_tile_position != previous_tile_position):
					place_diagonal_tile()
					previous_tile_position = hovering_tile_position
			
			# Horizontal/Vertical Line Placement
			if(holding_lmb && !place_rectangular && editor_selected == "wall"):
				if(hovering_tile_position != previous_tile_position):
					place_line_tile()
					previous_tile_position = hovering_tile_position
		
		if(Input.is_action_pressed("mouse_left")):
			if(previous_tile_position != hovering_tile_position):
				holding_lmb = true
				if(place_diagonal):
					tile_array.append(hovering_tile_position)
				add_tile(hovering_tile_position, 0)
				# place_ground(hovering_tile_position)
				previous_tile_position = hovering_tile_position # prevents looping while we are in the same tile
		
		# LMB Released
		elif(event.is_action_released("mouse_left")):
			holding_lmb = false
			tile_array.clear()
			direction = Vector2(0, 0)
			
			# Door Placement
			if(editor_selected == "door"):
				place_door(hovering_tile_position)
		
		if(event.is_action_pressed("left_ctrl")):
			place_rectangular = true
		elif(event.is_action_released("left_ctrl")):
			place_rectangular = false
		
		if(event.is_action_pressed("left_shift")):
			if(holding_lmb):
				tile_array.append(hovering_tile_position)
			place_diagonal = true
		elif(event.is_action_released("left_shift")):
			place_diagonal = false
		
		
		# RIGHT MOUSE BUTTON - Remove existing tile
		if(Input.is_action_pressed("mouse_right")):
			if(!can_build(hovering_tile_position)):
				if(editor_selected == "door" || editor_selected == "wall"):
					remove_wall(hovering_tile_position)
					#preview_block_placement()
			
			if(editor_selected == "floor"):
				remove_ground(hovering_tile_position)

		# ESC - Turn off build mode
		if(event.is_action_pressed("button_esc")):
			set_active(false)
			var console = get_node("../console")
			console.execute("build 0") # This will turn build mode off
		
	if(event.is_action_pressed("print")):
		var new_array = []
		
		var cell_vec2_array = editor_selections["floor"].get_used_cells()
		
		for index in range(0, cell_vec2_array.size()):
			
			# Add Cell ID
			new_array.append(editor_selections["floor"].get_cellv(cell_vec2_array[index]))
			
			# Add Vec2
			new_array.append(cell_vec2_array[index])
		
		print(str(new_array))
		

# Used by Player to check if standing on door tile
func get_tile_id_from_pos(vec2_pos):
	return editor_selections["floor"].get_cellv(vec2_pos)

# Places a rectangle floor
func place_rectangle_floor():
	var current_vector = Vector2(hovering_tile_position.x - tile_array[0].x, hovering_tile_position.y - tile_array[0].y) # Distance in X and Y from origin (first tile)
	for x in range(abs(current_vector.x) + 1):
		if(current_vector.x < 0):
			add_tile(Vector2(hovering_tile_position.x + x, hovering_tile_position.y), 0)
		else:
			add_tile(Vector2(hovering_tile_position.x - x, hovering_tile_position.y), 0)
	
	for y in range(abs(current_vector.y) + 1):
		
		if(current_vector.y < 0):
			add_tile(Vector2(hovering_tile_position.x, hovering_tile_position.y + y), 0)
		else:
			add_tile(Vector2(hovering_tile_position.x, hovering_tile_position.y - y), 0)


# Places tiles diagonally (walls and floor)
func place_diagonal_tile():
	# On first run we get initial direction (on both axis)
		if(tile_array.size() == 0):
			var temp_direction = hovering_tile_position - previous_tile_position
			temp_direction = temp_direction.normalized()
			
			# Assign direction once we have one
			if(direction.x == 0 && abs(temp_direction.x) == 1):
				direction.x = temp_direction.x
			
			if(direction.y == 0 && abs(temp_direction.y) == 1):
				direction.y = temp_direction.y
			
			# If we have gotten a 1 from X and Y direction; 
			if(abs(direction.x) == 1 && abs(direction.y) == 1):
				tile_array.append(hovering_tile_position)
				add_tile(tile_array[0], 0)
		else:
			
			var current_vector = Vector2(hovering_tile_position.x - tile_array[0].x, hovering_tile_position.y - tile_array[0].y)							
			var distance_from_origin = int(current_vector.length())
			var final_vector = tile_array[0]

			# NE/SW?
			if(direction.x == 1 && direction.y == -1 || direction.x == -1 && direction.y == 1):
				add_tile(Vector2(tile_array[0].x - current_vector.y, tile_array[0].y + current_vector.y), 0)
			# NW/SE
			elif(direction.x == -1 && direction.y == -1 || direction.x == 1 && direction.y == 1):
				add_tile(Vector2(tile_array[0].x + current_vector.y, tile_array[0].y + current_vector.y), 0)

# Places walls in a line
func place_line_tile():
	if(tile_array.size() == 0):
		# Get the direction, direction = (After - Before)
		direction = hovering_tile_position - previous_tile_position
		direction = direction.normalized()
		tile_array.append(hovering_tile_position) # Add initial (first) tile vec2
		
		# Second tile (first happens at the moment of pressing LMB)
		# This tile locks the axis for placing tiles
		add_tile(hovering_tile_position, 0)
	else:
		if(abs(direction.y) == 1):
			add_tile(Vector2(tile_array[0].x, hovering_tile_position.y), 0)
		elif(abs(direction.x) == 1):
			add_tile(Vector2(hovering_tile_position.x, tile_array[0].y), 0)

# Adds tile according to which we have selected in the editor
func add_tile(vec2_pos, num):
	if(editor_selected == "floor"):
		place_ground(vec2_pos)
	elif(editor_selected == "wall"):
		place_wall(vec2_pos, 0)
	elif(editor_selected == "door"):
		place_door(vec2_pos)


# Returns an array with tile IDs of neighboring tiles,
# starting from Top Left and going clockwise from 1 to 8.
# [01][02][03]
# [08][##][04]
# [07][06][05]
func get_neighbors(tile_vec2):
	
	var selection = "floor"
	var neighbors = Array()
	
	# Top Left
	neighbors.append(editor_selections[selection].get_cellv(Vector2(tile_vec2.x-1, tile_vec2.y-1)))
	
	# Top
	neighbors.append(editor_selections[selection].get_cellv(Vector2(tile_vec2.x, tile_vec2.y-1)))
	
	# Top Right
	neighbors.append(editor_selections[selection].get_cellv(Vector2(tile_vec2.x+1, tile_vec2.y-1)))
	
	# Right
	neighbors.append(editor_selections[selection].get_cellv(Vector2(tile_vec2.x+1, tile_vec2.y)))
	
	# Bottom Right
	neighbors.append(editor_selections[selection].get_cellv(Vector2(tile_vec2.x+1, tile_vec2.y+1)))
	
	# Bottom
	neighbors.append(editor_selections[selection].get_cellv(Vector2(tile_vec2.x, tile_vec2.y+1)))
	
	# Bottom Left
	neighbors.append(editor_selections[selection].get_cellv(Vector2(tile_vec2.x-1, tile_vec2.y+1)))
	
	# Left
	neighbors.append(editor_selections[selection].get_cellv(Vector2(tile_vec2.x-1, tile_vec2.y)))
	
	# Return neighbors containing tile index for neighboring tiles
	return neighbors




# The act of placing an individual tile
func place_ground(tile_vec2):
	
	var selection = "floor"
	
	# Get neighboring tiles from middle tile
	var neighbors = get_neighbors(tile_vec2)
	
	# Middle
	# [__][__][__]
	# [__][15][__]
	# [__][__][__]
	var tile_id = 15
	editor_selections[selection].set_cellv(tile_vec2, tile_id)
	
	# Top Left
	# [04][__][__]
	# [__][##][__]
	# [__][__][__]
	var tile_id = 4 # Default tile
	if(neighbors[0] != -1): # If tile is NOT empty
		tile_id = neighbors[0] | 4
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x-1, tile_vec2.y-1), tile_id)
	
	# Top
	# [##][12][__]
	# [__][##][__]
	# [__][__][__]
	var tile_id = 12
	if(neighbors[1] != -1):
		tile_id = neighbors[1] | 4
		tile_id = tile_id | 8
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x, tile_vec2.y-1), tile_id)
	
	# Top Right
	# [##][##][08]
	# [__][##][__]
	# [__][__][__]
	var tile_id = 8
	if(neighbors[2] != -1): 
		tile_id = neighbors[2] | 8
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x+1, tile_vec2.y-1), tile_id)
	
	# Right
	# [##][##][##]
	# [__][##][09]
	# [__][__][__]
	var tile_id = 9
	if(neighbors[3] != -1):
		tile_id = neighbors[3] | 1
		tile_id = tile_id | 8
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x+1, tile_vec2.y), tile_id)
	
	# Bottom Right
	# [##][##][##]
	# [__][##][##]
	# [__][__][01]
	var tile_id = 1
	if(neighbors[4] != -1):
		tile_id = neighbors[4] | 1
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x+1, tile_vec2.y+1), tile_id)
	
	# Bottom
	# [##][##][##]
	# [__][##][##]
	# [__][03][##]
	var tile_id = 3
	if(neighbors[5] != -1):
		tile_id = neighbors[5] | 1
		tile_id = tile_id | 2
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x, tile_vec2.y+1), tile_id)
	
	# Bottom Left
	# [##][##][##]
	# [__][##][##]
	# [02][##][##]
	var tile_id = 2
	if(neighbors[6] != -1):
		tile_id = neighbors[6] | 2
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x-1, tile_vec2.y+1), tile_id)
	
	# Left
	# [##][##][##]
	# [06][##][##]
	# [##][##][##]
	var tile_id = 6
	if(neighbors[7] != -1):
		tile_id = neighbors[7] | 2
		tile_id = tile_id | 4
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x-1, tile_vec2.y), tile_id)
	
	# Send data to server
	if(has_node("/root/root_node/net")):
		if(!net_place_ground):
			var net = get_node("/root/root_node/net")
			if(net.peer.name == "client"):
				net.add_ground([tile_vec2])
		
	net_place_ground = false # hack

# Remove ground tiles
func remove_ground(tile_vec2):
	# Get neighboring tiles from middle tile
	var neighbors = get_neighbors(tile_vec2)
	var tile_id # Storage
	var selection = "floor"
	
	# Middle
	# [__][__][__]
	# [__][15][__]
	# [__][__][__]
	editor_selections[selection].set_cellv(tile_vec2, -1)
	
	# Top Left
	# [04][__][__]
	# [__][##][__]
	# [__][__][__]
	tile_id = neighbors[0]
	if(tile_id != -1):
		if(tile_id & 4):
			tile_id -= 4
		if(tile_id == 0):
			tile_id = -1
		editor_selections[selection].set_cellv(Vector2(tile_vec2.x-1, tile_vec2.y-1), tile_id)
	
	# Top
	# [##][12][__]
	# [__][##][__]
	# [__][__][__]
	tile_id = neighbors[1]
	if(tile_id != -1):
		if(tile_id & 4):
			tile_id -= 4
		if(tile_id & 8):
			tile_id -= 8
		if(tile_id == 0):
			tile_id = -1
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x, tile_vec2.y-1), tile_id)
	
	# Top Right
	# [##][##][08]
	# [__][##][__]
	# [__][__][__]
	tile_id = neighbors[2]
	if(tile_id != -1):
		if(tile_id & 8):
			tile_id -= 8
		if(tile_id == 0):
			tile_id = -1
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x+1, tile_vec2.y-1), tile_id)
	
	# Right
	# [##][##][##]
	# [__][##][09]
	# [__][__][__]
	tile_id = neighbors[3]
	if(tile_id != -1):
		if(tile_id & 1):
			tile_id -= 1
		if(tile_id & 8):
			tile_id -= 8
		if(tile_id == 0):
			tile_id = -1
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x+1, tile_vec2.y), tile_id)
	
	# Bottom Right
	# [##][##][##]
	# [__][##][##]
	# [__][__][01]
	tile_id = neighbors[4]
	if(tile_id != -1):
		if(tile_id & 1):
			tile_id -= 1
		if(tile_id == 0):
			tile_id = -1
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x+1, tile_vec2.y+1), tile_id)
	
	# Bottom
	# [##][##][##]
	# [__][##][##]
	# [__][03][##]
	tile_id = neighbors[5]
	if(tile_id != -1): 
		if(tile_id & 1):
			tile_id -= 1
		if(tile_id & 2):
			tile_id -= 2
		if(tile_id == 0):
			tile_id = -1
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x, tile_vec2.y+1), tile_id)
	
	# Bottom Left
	# [##][##][##]
	# [__][##][##]
	# [02][##][##]
	tile_id = neighbors[6]
	if(tile_id != -1): 
		if(tile_id & 2):
			tile_id -= 2
		if(tile_id == 0):
			tile_id = -1
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x-1, tile_vec2.y+1), tile_id)
	
	# Left
	# [##][##][##]
	# [06][##][##]
	# [##][##][##]
	tile_id = neighbors[7]
	if(tile_id != -1): 
		if(tile_id & 2):
			tile_id -= 2
		if(tile_id & 4):
			tile_id -= 4
		if(tile_id == 0):
			tile_id = -1
	editor_selections[selection].set_cellv(Vector2(tile_vec2.x-1, tile_vec2.y), tile_id)
	
	# Send data to server
	if(has_node("/root/root_node/net")):
		if(!net_remove_ground):
			var net = get_node("/root/root_node/net")
			if(net.peer.name == "client"):
				net.remove_ground([tile_vec2])
		
	net_remove_ground = false # hack


# Handle console changed state (enabled/disabled)
func on_console(console_enabled):
	building_enabled = !console_enabled
	self.console_enabled = console_enabled # We keep this so that we do not override building_enabled elsewhere

func on_button_hovering(is_hovering):
	if(!console_enabled):
		building_enabled = !is_hovering
	

# Used to enable or disable building mode
func set_active(state):
	is_active = state
	set_process_input(state)
	
	# Show or Hide Editor GUI
	if(state):
		editor_gui.show()
	else:
		editor_gui.hide()
	
	tilemap_info.set_cellv(preview_tile_pos, -1) # Clear building hovering tile
	
	# Inform those that needs to be informed about the changes
	var building_button = get_node("/root/root_node/GUI/HUD/building/build_button")
	if(state != building_button.is_pressed()):
		building_button.set_pressed(state)


# tile_vec2 : Vector2 for Tile coordinate
# num : current number of iteration (should be 0 at start, 1 on the last run)
# tilemap_name : String used to determine what to place
func place_wall(tile_vec2, num):
	if(editor_selected == "wall" || editor_selected == "door"):
		var matching_tile_found = false
		var rotation = 0
		var mirror = false
		
		while(!matching_tile_found):
			
			var nearby_tiles = get_nearby_tiles(tile_vec2, rotation, mirror)
			
			# Wall-000
			if(nearby_tiles == 00000000):
				set_tile("wall-000", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			# Wall-001
			if(nearby_tiles == 00000001):
				# Place tile
				set_tile("wall-001", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
				
			# Wall-002
			if(nearby_tiles == 00000010 || 
			nearby_tiles == 00000011 || 
			nearby_tiles == 00000110  || 
			nearby_tiles == 00000111):
				set_tile("wall-002", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			# Wall-003
			if(nearby_tiles == 00000101):
				set_tile("wall-003", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			# Wall-004
			if(nearby_tiles == 00010001 ||
			nearby_tiles == 00010101 ||
			nearby_tiles == 10010001 ||
			nearby_tiles == 10010101):
				set_tile("wall-004", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			# Wall-005
			if(nearby_tiles == 10000001):
				set_tile("wall-005", tile_vec2, rotation, mirror)
				# set_tile("wall-014", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			# Wall-006
			if(nearby_tiles == 00001010 ||
			nearby_tiles == 00001011 ||
			nearby_tiles == 00001110 ||
			nearby_tiles == 00001111 ||
			nearby_tiles == 00101010 ||
			nearby_tiles == 00101011 ||
			nearby_tiles == 00101110 ||
			nearby_tiles == 00101111 ):
				set_tile("wall-006", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			# Wall-007
			if(nearby_tiles == 01000010 ||
			nearby_tiles == 01000011 ||
			nearby_tiles == 01000110 ||
			nearby_tiles == 01000111 ||
			nearby_tiles == 01100010 ||
			nearby_tiles == 01100011 ||
			nearby_tiles == 01100110 ||
			nearby_tiles == 01100111 ||
			nearby_tiles == 11000010 ||
			nearby_tiles == 11000011 ||
			nearby_tiles == 11000110 ||
			nearby_tiles == 11000111 ||
			nearby_tiles == 11100010 ||
			nearby_tiles == 11100011 ||
			nearby_tiles == 11100110 ||
			nearby_tiles == 11100111):
				set_tile("wall-007", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			
			# Wall-008
			if(nearby_tiles == 00011010 ||
			nearby_tiles == 00011011 ||
			nearby_tiles == 00011110 ||
			nearby_tiles == 00011111 ||
			nearby_tiles == 00111010 ||
			nearby_tiles == 00111011 ||
			nearby_tiles == 00111110 ||
			nearby_tiles == 00111111 ||
			nearby_tiles == 10011010 || 
			nearby_tiles == 10011011 ||
			nearby_tiles == 10011110 ||
			nearby_tiles == 10011111 ||
			nearby_tiles == 10111010 ||
			nearby_tiles == 10111011 ||
			nearby_tiles == 10111110 ||
			nearby_tiles == 10111110 ||
			nearby_tiles == 10111111 ):
				set_tile("wall-008", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			# Wall-009
			if(nearby_tiles == 00100101):
				set_tile("wall-009", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			# Wall-010
			if(nearby_tiles == 01000101 ||
			nearby_tiles == 01100101 ||
			nearby_tiles == 11000101 ||
			nearby_tiles == 11100101 ):
				set_tile("wall-010", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			# Wall-011
			if(nearby_tiles == 00110010 ||
			nearby_tiles == 00110011 ||
			nearby_tiles == 00110110 ||
			nearby_tiles == 00110111 ||
			nearby_tiles == 10110010 ||
			nearby_tiles == 10110011 ||
			nearby_tiles == 10110110 ||
			nearby_tiles == 10110111):
				set_tile("wall-011", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			# Wall-012
			if(nearby_tiles == 01011010 ||
			nearby_tiles == 01011011 ||
			nearby_tiles == 01011110 ||
			nearby_tiles == 01011111 ||
			nearby_tiles == 01111010 ||
			nearby_tiles == 01111011 ||
			nearby_tiles == 01111110 ||
			nearby_tiles == 01111111 ||
			nearby_tiles == 11011010 ||
			nearby_tiles == 11011011 ||
			nearby_tiles == 11011110 ||
			nearby_tiles == 11011111 ||
			nearby_tiles == 11111010 ||
			nearby_tiles == 11111011 ||
			nearby_tiles == 11111110 ||
			nearby_tiles == 11111111 ):
				set_tile("wall-012", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			# Wall-013
			if(nearby_tiles == 10100101 ):
				set_tile("wall-013", tile_vec2, rotation, mirror)
				matching_tile_found = true
				continue;
			
			
			# No tile match; rotate and repeat
			rotation += 1
			
			# If we have rotated 360 degrees
			if(rotation >= 4):
				# If mirror is false, set it to true and check another 360 degrees for a match
				if(!mirror):
					mirror = true
					rotation = 0
				else:
					print("ERROR: NO MATCHING BLOCK FOUND! - THIS SHOULD NOT HAPPEN, AS EVERY POSSIBLE BLOCK COMBINATION SHOULD BE ACCOUNTED FOR") # debug
					break; # terminate loop without setting matching_tile_found to true (and thus not updating nearby tiles)
		if(matching_tile_found):
			# Since we have placed the initial tile; we update nearby tiles
			if(num == 0):
				update_nearby_tiles(tile_vec2, "wall")
				# Send data to server
				if(has_node("/root/root_node/net")):
					if(!net_place_wall):
						var net = get_node("/root/root_node/net")
						if(net.peer.name == "client"):
							net.add_wall([tile_vec2])
					
				net_place_wall = false # hack
		
		

# Remove walls and update neighbors
func remove_wall(tile_vec2):
	editor_selections["wall"].set_cellv(tile_vec2, -1)
	update_nearby_tiles(tile_vec2, "wall")
	
	# Send data to server that we are removing
	if(has_node("/root/root_node/net")):
		if(!net_remove_wall):
			var net = get_node("/root/root_node/net")
			if(net.peer.name == "client"):
				print("Sending Remove Wall Message from client to server")
				net.remove_wall([tile_vec2])
		
	net_remove_wall = false # hack



func place_door(tile_vec2):
	# First iteration
	var matching_tile_found = false
	var rotation = 0
	var mirror = false
	while(!matching_tile_found):
		
		var nearby_tiles = get_nearby_tiles(tile_vec2, rotation, mirror)

		# Door-001
		if(nearby_tiles == 01000010 ||
		nearby_tiles == 01000011 ||
		nearby_tiles == 01000110 ||
		nearby_tiles == 01000111 ||
		nearby_tiles == 01100010 ||
		nearby_tiles == 01100011 ||
		nearby_tiles == 01100110 ||
		nearby_tiles == 01100111 ||
		nearby_tiles == 11000010 ||
		nearby_tiles == 11000011 ||
		nearby_tiles == 11000110 ||
		nearby_tiles == 11000111 ||
		nearby_tiles == 11100010 ||
		nearby_tiles == 11100011 ||
		nearby_tiles == 11100110 ||
		nearby_tiles == 11100111):
			set_tile("door-001", tile_vec2, rotation, mirror)
			matching_tile_found = true
			continue;
		
		# No tile match; rotate and repeat
		rotation += 1
		
		# If we have rotated 360 degrees
		if(rotation >= 4):
			# If mirror is false, set it to true and check another 360 degrees for a match
			if(!mirror):
				mirror = true
				rotation = 0
			else:
				break; # terminate loop without setting matching_tile_found to true (and thus not updating nearby tiles)
	if(matching_tile_found):
		# Since we have placed the initial tile; we update nearby tiles
		update_nearby_tiles(tile_vec2, "wall")
		
		# Send data to server
		if(has_node("/root/root_node/net")):
			if(!net_place_door):
				var net = get_node("/root/root_node/net")
				if(net.peer.name == "client"):
					net.add_door([tile_vec2])
			
		net_place_door = false # hack



# Updates all placed tiles around the vec2_tile position.
# This is run after initially placing a new tile
# vec2_tile : Vector2 for the Tile
# tilemap_name : String name such as "wall", "door", ... 
func update_nearby_tiles(vec2_tile, tilemap_name):
	
	if(tilemap_name == "wall"):
		var nearby_tiles = get_decimal_from_binary(get_nearby_tiles(vec2_tile, 0, false))
		
		# Top Left
		if(nearby_tiles & 1):
			place_wall(vec2_tile + Vector2(-1, -1), 1)
		
		# Top
		if(nearby_tiles & 2):
			place_wall(vec2_tile + Vector2(0, -1), 1)
		
		# Top Right
		if(nearby_tiles & 4):
			place_wall(vec2_tile + Vector2(1, -1), 1)
	
		# Left
		if(nearby_tiles & 8):
			place_wall(vec2_tile + Vector2(-1, 0), 1)
		
		# Right
		if(nearby_tiles & 16):
			place_wall(vec2_tile + Vector2(1, 0), 1)
	
		# Bottom Left
		if(nearby_tiles & 32):
			place_wall(vec2_tile + Vector2(-1, 1), 1)
		
		# Bottom
		if(nearby_tiles & 64):
			place_wall(vec2_tile + Vector2(0, 1), 1)
		
		# Bottom Right
		if(nearby_tiles & 128):
			place_wall(vec2_tile + Vector2(1, 1), 1)


# Returns true if current tile can be placed on the vec2_pos (tile)
func can_build(vec2_pos):
	return (editor_selections[editor_selected].get_cellv(vec2_pos) == -1)




# Set a tile on current tilemap
# --
# tile_name is the name of the tile
# tile_pos is the Vec2 position for the tile we are going to set
# rotation is the rotation number * 90 degrees
# mirror determines whether we are going to mirror the block
func set_tile(tile_name, tile_pos, rotation, mirror):
	
	var flip_x = (rotation == 0 && mirror)
	var flip_y = false
	var transpose = false

	# All rotations are done clockwise. Mirrored are also rotated clockwise
	if(rotation == 1): # 90 degrees
		flip_x = true
		flip_y = mirror
		transpose = true
	elif(rotation == 2): # 180 degrees
		flip_x = !mirror
		flip_y = true
	elif(rotation == 3): # 270 degrees
		flip_y = !mirror
		transpose = true
		
	editor_selections[editor_selected].set_cellv(tile_pos, editor_selections[editor_selected].get_tileset().find_tile_by_name(tile_name), flip_x, flip_y, transpose)


# Returns true if there is a tile placed at vec2_tile
func has_tile(vec2_tile):
	return editor_selections[editor_selected].get_cell(vec2_tile.x, vec2_tile.y) != -1


# Return a binary value containing data on neighboring tiles
# Rotation is used to determine which direction we are checking tiles
func get_nearby_tiles(vec2_tile, rotation, mirror):
	
	var top_left = has_tile(vec2_tile + Vector2(-1, -1))
	var top = has_tile(vec2_tile + Vector2(0, -1))
	var top_right = has_tile(vec2_tile + Vector2(1, -1))
	var left = has_tile(vec2_tile + Vector2(-1, 0))
	var right = has_tile(vec2_tile + Vector2(1, 0))
	var bottom_left = has_tile(vec2_tile + Vector2(-1, 1))
	var bottom = has_tile(vec2_tile + Vector2(0, 1))
	var bottom_right = has_tile(vec2_tile + Vector2(1, 1))
	
	# If we are checking for mirrored, we just flip right with left, left with right
	if(rotation == 0 && mirror):
		top_right = has_tile(vec2_tile + Vector2(-1, -1))
		top = has_tile(vec2_tile + Vector2(0, -1))
		top_left = has_tile(vec2_tile + Vector2(1, -1))
		right = has_tile(vec2_tile + Vector2(-1, 0))
		left = has_tile(vec2_tile + Vector2(1, 0))
		bottom_right = has_tile(vec2_tile + Vector2(-1, 1))
		bottom = has_tile(vec2_tile + Vector2(0, 1))
		bottom_left = has_tile(vec2_tile + Vector2(1, 1))
	
	# Rotation 90 degrees towards the right
	if(rotation == 1):
		if(!mirror):
			top_left = has_tile(vec2_tile + Vector2(1, -1)) # 1
			top = has_tile(vec2_tile + Vector2(1, 0)) # 2
			top_right = has_tile(vec2_tile + Vector2(1, 1)) # 4
			left = has_tile(vec2_tile + Vector2(0, -1)) # 8 
			right = has_tile(vec2_tile + Vector2(0, 1)) # 16
			bottom_left = has_tile(vec2_tile + Vector2(-1, -1)) # 32 
			bottom = has_tile(vec2_tile + Vector2(-1, 0)) # 64
			bottom_right = has_tile(vec2_tile + Vector2(-1, 1)) # 128 
		else:
			top_right = has_tile(vec2_tile + Vector2(1, -1)) # 1
			top = has_tile(vec2_tile + Vector2(1, 0)) # 2
			top_left = has_tile(vec2_tile + Vector2(1, 1)) # 4
			right = has_tile(vec2_tile + Vector2(0, -1)) # 8 
			left = has_tile(vec2_tile + Vector2(0, 1)) # 16
			bottom_right = has_tile(vec2_tile + Vector2(-1, -1)) # 32 
			bottom = has_tile(vec2_tile + Vector2(-1, 0)) # 64
			bottom_left = has_tile(vec2_tile + Vector2(-1, 1)) # 128 
	
	# Rotation 180 degrees towards the right
	if(rotation == 2):
		if(!mirror):
			top_left = has_tile(vec2_tile + Vector2(1, 1)) # 1
			top = has_tile(vec2_tile + Vector2(0, 1)) # 2
			top_right = has_tile(vec2_tile + Vector2(-1, 1)) # 4
			left = has_tile(vec2_tile + Vector2(1, 0)) # 8 
			right = has_tile(vec2_tile + Vector2(-1, 0)) # 16
			bottom_left = has_tile(vec2_tile + Vector2(1, -1)) # 32 
			bottom = has_tile(vec2_tile + Vector2(0, -1)) # 64
			bottom_right = has_tile(vec2_tile + Vector2(-1, -1)) # 128 
		else:
			top_right = has_tile(vec2_tile + Vector2(1, 1)) # 1
			top = has_tile(vec2_tile + Vector2(0, 1)) # 2
			top_left = has_tile(vec2_tile + Vector2(-1, 1)) # 4
			right = has_tile(vec2_tile + Vector2(1, 0)) # 8 
			left = has_tile(vec2_tile + Vector2(-1, 0)) # 16
			bottom_right = has_tile(vec2_tile + Vector2(1, -1)) # 32 
			bottom = has_tile(vec2_tile + Vector2(0, -1)) # 64
			bottom_left = has_tile(vec2_tile + Vector2(-1, -1)) # 128 
	
	# Rotation 270 degrees towards the right
	if(rotation == 3):
		if(!mirror):
			top_left = has_tile(vec2_tile + Vector2(-1, 1)) # 1
			top = has_tile(vec2_tile + Vector2(-1, 0)) # 2
			top_right = has_tile(vec2_tile + Vector2(-1, -1)) # 4
			left = has_tile(vec2_tile + Vector2(0, 1)) # 8 
			right = has_tile(vec2_tile + Vector2(0, -1)) # 16
			bottom_left = has_tile(vec2_tile + Vector2(1, 1)) # 32 
			bottom = has_tile(vec2_tile + Vector2(1, 0)) # 64
			bottom_right = has_tile(vec2_tile + Vector2(1, -1)) # 128 
		else:
			top_right = has_tile(vec2_tile + Vector2(-1, 1)) # 1
			top = has_tile(vec2_tile + Vector2(-1, 0)) # 2
			top_left = has_tile(vec2_tile + Vector2(-1, -1)) # 4
			right = has_tile(vec2_tile + Vector2(0, 1)) # 8 
			left = has_tile(vec2_tile + Vector2(0, -1)) # 16
			bottom_right = has_tile(vec2_tile + Vector2(1, 1)) # 32 
			bottom = has_tile(vec2_tile + Vector2(1, 0)) # 64
			bottom_left = has_tile(vec2_tile + Vector2(1, -1)) # 128 
	
	
	# Add value from each existing tile
	var decimal = 0
	if(top_left):
		decimal += 1
	if(top):
		decimal += 2
	if(top_right):
		decimal += 4
	if(left):
		decimal += 8
	if(right):
		decimal += 16
	if(bottom_left):
		decimal += 32
	if(bottom):
		decimal += 64
	if(bottom_right):
		decimal += 128

	# Returns the binary value for neighboring tiles
	return get_binary_from_decimal(decimal)


# Converts a decimal value to a binary value and returns it
func get_binary_from_decimal(var decimal_value):
	var binary_string = ""
	var temp
	var count = 31 # Checking up to 32 bits

	while(count >= 0):
		temp = decimal_value >> count
		if(temp & 1):
			binary_string = binary_string + "1"
		else:
			binary_string = binary_string + "0"
		count -= 1

	# Return the binary value as an int
	return int(binary_string)


# Converts a binary value to a decimal value and returns it
func get_decimal_from_binary(var binary_value):
	var decimal_value = 0
	var count = 0
	var temp

	while(binary_value != 0):
		temp = binary_value % 10
		binary_value /= 10
		decimal_value += temp * pow(2, count)
		count += 1

	# Return decimal value
	return int(decimal_value)



var preview_tile_pos = Vector2(-1, -1) # Used to prevent looping when inside the same tile
# When this is running; will display the currently selected block
func preview_block_placement():
	# As long as the current hovering tile position is not equal to previous
	if(preview_tile_pos != hovering_tile_position):
		# As long as we can build on this tile
		if(can_build(hovering_tile_position)):
			# Draw green
			if(editor_selected == "floor"):
				tilemap_info.set_scale(Vector2(0.5, 0.5))
			else:
				tilemap_info.set_scale(Vector2(1, 1))
	
			tilemap_info.set_cellv(hovering_tile_position, 1)
		else:
			# Draw red
			if(editor_selected == "floor"):
				tilemap_info.set_scale(Vector2(0.5, 0.5))
			else:
				tilemap_info.set_scale(Vector2(1, 1))
				
			tilemap_info.set_cellv(hovering_tile_position, 2) # Display red

		# If we have moved (and placed another tile), we remove last tile so that we only have 1 at a time
		# TODO: Find a better way of handling this?
		if(tilemap_info.get_used_cells().size() > 1):
			tilemap_info.set_cellv(preview_tile_pos, -1) # Blank out color tile
			pass
		preview_tile_pos = hovering_tile_position # Set current position as previous to prevent looping
		

# NETWORKING
# Server uses this to get an world array to send to the clients
func get_world():
	# FLOOR
	var floor_array = [] # Empty array for floors
	var floor_cells = editor_selections["floor"].get_used_cells() # Vec2 for all placed tiles in Floor tilemap
	
	# For each Vec2, we add a tile id and vec2 to a new array
	for index in range(0, floor_cells.size()):
		var cell_id = editor_selections["floor"].get_cellv(floor_cells[index])
		var vec2_pos = floor_cells[index]
		floor_array.append(cell_id)
		floor_array.append(vec2_pos)
	
	
	# WALLS
	var wall_array = [] # Empty array for walls
	var wall_cells = editor_selections["wall"].get_used_cells() 
	
	# For each Vec2, we add a tile id and vec2 to a new array
	for index in range(0, wall_cells.size()):
		var cell_id = editor_selections["wall"].get_cellv(wall_cells[index])
		var vec2_pos = wall_cells[index]
		var flip_x = editor_selections["wall"].is_cell_x_flipped(vec2_pos.x, vec2_pos.y)
		var flip_y = editor_selections["wall"].is_cell_y_flipped(vec2_pos.x, vec2_pos.y)
		var transpose = editor_selections["wall"].is_cell_transposed(vec2_pos.x, vec2_pos.y)
		wall_array.append(cell_id)
		wall_array.append(vec2_pos)
		wall_array.append(flip_x)
		wall_array.append(flip_y)
		wall_array.append(transpose)
	
	return [floor_array, wall_array] # Return an array of tile arrays

# LOAD WORLD THAT WE RECIEVED FROM SERVER
func client_load_world(world_array):
	var floor_array = world_array[0]
	var wall_array = world_array[1]
	
	
	# Load Floor Tilemap
	for index in range(0, floor_array.size(), 2):
		var tile_id = floor_array[index]
		var vec2_pos = floor_array[index+1]
		editor_selections["floor"].set_cellv(vec2_pos, tile_id)
	
	# Load Wall Tilemap
	for index in range(0, wall_array.size(), 5):
		var tile_id = wall_array[index]
		var vec2_pos = wall_array[index+1]
		var flip_x = wall_array[index+2]
		var flip_y = wall_array[index+3]
		var transpose = wall_array[index+4]
		editor_selections["wall"].set_cellv(vec2_pos, tile_id, flip_x, flip_y, transpose)

# ADD GROUND
func server_place_ground(tile_vec2):
	place_ground(tile_vec2) # Server runs this on self

var net_place_ground = false
func client_place_ground(tile_vec2):
	net_place_ground = true
	place_ground(tile_vec2) # Server runs this on self

# REMOVE GROUND
func server_remove_ground(tile_vec2):
	remove_ground(tile_vec2)

var net_remove_ground = false
func client_remove_ground(tile_vec2):
	net_remove_ground = true
	remove_ground(tile_vec2) # Server runs this on self

# ADD WALL
func server_place_wall(tile_vec2):
	place_wall(tile_vec2, 0) # Server runs this on self

var net_place_wall = false
func client_place_wall(tile_vec2):
	net_place_wall = true
	place_wall(tile_vec2, 0) # Server runs this on self

# REMOVE WALL
func server_remove_wall(tile_vec2):
	remove_wall(tile_vec2)

var net_remove_wall = false
func client_remove_wall(tile_vec2):
	net_remove_wall = true
	remove_wall(tile_vec2) # Server runs this on self

# ADD DOOR
func server_place_door(tile_vec2):
	place_door(tile_vec2) # Server runs this on self

var net_place_door = false
func client_place_door(tile_vec2):
	net_place_door = true
	place_door(tile_vec2) # Server runs this on self