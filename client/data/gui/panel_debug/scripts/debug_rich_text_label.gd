
extends RichTextLabel

func _ready():
	set_process(true)

# Update debugging data
func _process(delta):
	clear()
	add_text("FPS: " + str(OS.get_frames_per_second()) + "\n")
	# add_text("Frames Drawn: " + str(OS.get_frames_drawn()) + "\n")
	add_text("Window size: " + str(OS.get_window_size()) + "\n")
	add_text("Screen size: " + str(OS.get_screen_size()) + "\n")
	
	# Network statistics (hack)
	if (has_node("/root/root_node/net")):
		var net = get_node("/root/root_node/net")
		add_text(net.peer.statistics.to_string())
		if (net.peer.name == "client"):
			var server = net.peer.get_server_connection()
			if (server != null):
				add_text("Connected: " + str(server.is_connected()) + "\n")
				add_text("Avg rtt: " + str(server.avg_rtt) + "\n")
			else:
				add_text("No server connection")
		else:
			add_text("Connections: " + str(net.peer.connections.size()) + "\n")