extends Reference

# Command template. New commands extends this.
const CMD_TYPE = {
TOGGLE = 0,
COMMAND = 1, 
VARIABLE = 2
}

var name # Command name
var value # Current value
var type # Command type
var expected_values  = [] # Expected input values

var node_root # Root node reference

# Returns true of input matches name
func matches(input):
	return input.begins_with(name)

# Check if the command is valid; returns an error if it is not valid
# TODO: Handle multiple command types (currently only supporting TOGGLE On-Off commands)
func execute(input):
	var raw_input = input
	input = input.replace(name, "") # Remove the command name from the string
	input = input.strip_edges() # Remove any leading or ending whitespace
	
	# If we find a matching value we have a valid command
	if(type == CMD_TYPE.TOGGLE):
		if(expected_values.find(input) != -1):
			# No error has been found
			print("--------------------------------------------")
			print("command.gd -> text input: " + str(raw_input))
			print("--------------------------------------------")
			return perform_action(input)
		else:
			print("--------------------------------------------")
			print("Command Error:")
			print("\"" + str(input) + "\" is not a valid value.\nExpected values: {" + str(expected_values) + "}\nEx: " + str(name) + " " + str(expected_values[0]))
			print("--------------------------------------------")
			return "\"" + str(input) + "\" is not a valid value.\nExpected values: {" + str(expected_values) + "}\nEx: " + str(name) + " " + str(expected_values[0])
	# Not currently in use
	elif(type == CMD_TYPE.VARIABLE):
		if(input != ""):
			return perform_action(input)
		else:
			return "Expected integer. Parameters cannot be empty."
	elif(type == CMD_TYPE.COMMAND):
		return perform_action(input)

# This is overwritten; use this to perform command actions
func perform_action(input):
	pass
