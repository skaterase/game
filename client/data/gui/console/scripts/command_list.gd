extends Reference

# All Commands
var debug
var commands = []
var node_root # Reference to root node

func _init(node):
	node_root = node
	load_commands()

# Find command and execute it
func execute(input):
	if(commands.size() != 0):
		for cmd in commands:
			if(cmd.matches(input)):
				return cmd.execute(input) # Return result message (if any)
		return "Unknown command \"" + str(input) + "\""
	else:
		return("This should never happen, as there should be commands implemented!")

# Load all commands in given directory
func load_commands():
	# Get path to /commands/ folder
	var dir = self.get_script().get_path()
	dir = str(dir).replace("command_list.gd", "commands/")
	
	
	var directory = Directory.new()
	# Possible BUG
	# When exporting (pck, zip or exe) this returns false
	# even tough directory.open(dir) works
	# if (! directory.dir_exists(dir)):
	# 	print("Directory ", dir, " does not exist")
	# 	return
	if (directory.open(dir) == OK):
		directory.list_dir_begin()
		var file_name = directory.get_next()
		while(file_name != ""):
			if (!directory.current_is_dir()):
				print("Loading command: ", file_name)
				commands.append(load(dir + file_name).new(node_root))
			file_name = directory.get_next()
		directory.list_dir_end()
	else:
		print("Command directory \"", dir, "\" could not be opened")
