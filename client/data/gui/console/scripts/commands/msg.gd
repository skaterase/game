
extends "../command.gd"

func _init(node):
	name = "msg" # Command name
	type = CMD_TYPE.COMMAND # Command type
	expected_values  = [] # Expected input values
	node_root = node # Root node reference

# Spawn offline player
func perform_action(input):
	var global = node_root.get_node("/root/global")
	if (input != null):
		global.chat(input)
