extends "../command.gd"

func _init(node):
	name = "spawn" # Command name
	type = CMD_TYPE.COMMAND # Command type
	expected_values  = [] # Expected input values
	node_root = node # Root node reference

# Spawn offline player
func perform_action(input):
	var world = node_root.get_tree().get_root().get_node("root_node/world")
	if (! world.has_node("offline")):
		var player = world.spawn_offline_player("offline")
		player.console_enabled = true
		return "Spawned offline player"
	else:
		return "Offline player already spawned"
