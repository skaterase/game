# Hierarchy: debug.gd => command.gd
extends "../command.gd"

# Nodes
const panel_debug_prefab = preload("res://data/gui/panel_debug/panel_debug.tscn")
var panel_debug

# Data in
func _init(node):
	node_root = node # get gui canvas reference
	name = "debug" # name
	value = false  # default value/state
	type = CMD_TYPE.TOGGLE # type of command
	expected_values = ["0", "false", "1", "true", ""] # expected values (if any)

# Overwrite command.gd's function
func perform_action(input):
	# Perform action
	if((input == expected_values[0] || input == expected_values[1]) && value):
		value = false # Update current value
	elif((input == expected_values[2] || input == expected_values[3]) && !value):
		value = true # Update current value
	elif(input == expected_values[4]):
		# Toggle values
		value = !value
	else:
		return "" # Return empty string as no changes will be made
	
	# Update panel by creating it or removing it
	update_panel()
	
	# Return message
	if(value):
			return "Debugging have been enabled"
	else:
		return "Debugging have been disabled"


# Updates the panel - Creating new instance or removing it
func update_panel():
	if(value):
		# Create an instance of the debug panel scene and add it to canvas
		panel_debug = panel_debug_prefab.instance()
		node_root.add_child(panel_debug)
	else:
		# Remove the instance
		node_root.remove_child(panel_debug)
	