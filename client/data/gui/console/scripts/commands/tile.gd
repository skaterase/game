# Hierarchy: build.gd => command.gd
extends "../command.gd"

func _init(node):
	node_root = node # get gui canvas reference
	name = "tile" # name
	value = 0  # default value/state
	type = CMD_TYPE.VARIABLE # type of command
	# expected_values = ["0", "false", "1", "true", ""] # expected values (if any)

# Overwrite command.gd's function
func perform_action(input):
	# Update value
	value = int(input)
	
	# Update panel by creating it or removing it
	update()
	
	# Return message
	return ""


# Update building
func update():
	var world = node_root.get_node("/root/root_node/world")
	world.set_current_tile(value)