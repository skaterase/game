# Hierarchy: build.gd => command.gd
extends "../command.gd"


# Data in
func _init(node):
	node_root = node # get gui canvas reference
	name = "build" # name of the command (which is typed in console)
	value = false  # current state
	type = CMD_TYPE.TOGGLE # type of command
	expected_values = ["0", "false", "1", "true", ""] # ALL possible values for CMD_TYPE.TOGGLE

# Overwrite command.gd's function
func perform_action(input):
	# Perform action
	if((input == expected_values[0] || input == expected_values[1]) && value):
		value = false
	elif((input == expected_values[2] || input == expected_values[3]) && !value):
		value = true
	elif(input == expected_values[4]):
		value = !value # Toggle value when given empty parameter (from excpected_values)
	else:
		return "" # Return empty string to inform command.gd something went wrong
	
	# Update panel by creating it or removing it
	update()
	
	# Return message
	if(value):
		return "Building have been enabled"
	else:
		return "Building have been disabled"


# Update building
func update():
	var build_mode = node_root.get_node("/root/root_node/GUI/building")
	build_mode.set_active(value)

# Returns the value
func value():
	return self.value