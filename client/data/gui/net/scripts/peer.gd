extends Control


const MAX_PORT_NUMBER = 65535 # Highest possible listening port

var host = null # references instance of client or server

func _ready():
	# Set toggle mode on buttons
	get_node("toggle_client").set_toggle_mode(true)
	get_node("toggle_server").set_toggle_mode(true)
	# Connect signals to buttons
	get_node("toggle_client").connect("toggled", self, "_on_toggle_client")
	get_node("toggle_server").connect("toggled", self, "_on_toggle_server")

# Handle updated stats
func _on_stats_updated(text):
	get_node("statistics").set_text(text)

# Handle client/server start
func _on_start(host, is_listening):
	if (! is_listening):
		return
	
	if (host.name == "server"):
		# Unset focus on the active toggle button to avoid it receiving key presses
		get_node("toggle_server").set_focus_mode(FOCUS_NONE)
		# Disable client button
		get_node("toggle_client").set_disabled(true)
		# Set server name
		get_node("name").set_text("Name: server")
	else:
		# Unset focus on the active toggle button to avoid it receiving key presses
		get_node("toggle_client").set_focus_mode(FOCUS_NONE)
		# Disable server button
		get_node("toggle_server").set_disabled(true)
		# Set client name
		get_node("name").set_text("Name: " + host.name)

# Handle client/server stop
func _on_stop(host):
	disconnect_handlers(host)
	# Untoggle button
	if (host.name == "server"):
		# Enable client button
		get_node("toggle_client").set_disabled(false)
		# Untoggle server button
		get_node("toggle_server").set_pressed(false)
	else:
		# Enable server button
		get_node("toggle_server").set_disabled(false)
		# Untoggle client button
		get_node("toggle_client").set_pressed(false)
	get_node("statistics").set_text("")
	get_node("name").set_text("")

# Handle client toggle
func _on_toggle_client(pressed):
	if (host == null || host.name == "server"):
		var config = get_node("/root/global").get_config()
		host = Client.new(config)
	toggle(pressed)

# Handle server toggle
func _on_toggle_server(pressed):
	if (host == null || host.name != "server"):
		var config = get_node("/root/global").get_config()
		host = Server.new(config)
	toggle(pressed)

# Handle toggle by server or client
func toggle(pressed):
	if (! pressed):
		stop()
	else:
		host.reset()
		connect_handlers(host)
		add_child(host)
		if (host.name == "server"):
			start_server()
		else:
			start_client()
		

# Start server
func start_server():
	host.start()
	if (! host.is_processing()):
		# Host not processing so untoggle button
		get_node("toggle_server").set_pressed(false)
		get_node("name").set_text("Failed to start server")
	
# Start client
func start_client():
	while (! host.is_processing()):
		host.start()
		host.port += 1
		if (host.port > MAX_PORT_NUMBER):
			break
	if (! host.is_processing()):
		# Host not processing so untoggle button
		get_node("toggle_client").set_pressed(false)
		get_node("name").set_text("Failed to start client")

# Stop client/server
func stop():
	host.stop()
	remove_child(host)
