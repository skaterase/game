
extends TextureButton

onready var node_building = get_node("/root/root_node/GUI/building")
onready var console = get_node("/root/root_node/GUI/console")

func _ready():
	# Setup signleton that prevents user from placing as he clicks the button
	self.connect("mouse_enter", node_building, "on_button_hovering", [ true ])
	self.connect("mouse_exit", node_building, "on_button_hovering", [ false ])
	
	# node_building.connect("set_active", node_building, "toggled", [ false ])


func _toggled(pressed):
	if(pressed):
		# Enable Build mode
		console.execute("build 1")
	else:
		# Disable Build mode
		console.execute("build 0")