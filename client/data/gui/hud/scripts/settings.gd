
extends Button

onready var node_building = get_node("/root/root_node/GUI/building")
onready var parent = get_node("/root/root_node/GUI/HUD")
onready var menu_scene = preload("res://data/ingame_menu/ingame_menu.tscn")
onready var console = get_node("/root/root_node/GUI/console")


func _ready():
	self.connect("mouse_enter", node_building, "on_button_hovering", [ true ])
	self.connect("mouse_exit", node_building, "on_button_hovering", [ false ])

func _pressed():
	# If building is enabled; close it
	console.execute("build 0")

		# Open new instance of menu
	var instance = menu_scene.instance()
	parent.add_child(instance) # ERROR HERE