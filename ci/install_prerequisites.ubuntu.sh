#!/bin/bash

sudo apt-get update -y

# For building godot engine
sudo apt-get install -y \
    scons \
    pkg-config \
    libx11-dev \
    libxcursor-dev \
    build-essential \
    libasound2-dev \
    libpulse-dev \
    libfreetype6-dev \
    libgl1-mesa-dev \
    libglu-dev \
    libssl-dev \
    libxinerama-dev \
    libudev-dev \
    mingw32 \
    mingw-w64

# packer for executables
sudo apt-get install -y upx-ucl

# Need git to download repositories
sudo apt-get install -y git

# Install gitlab runner 
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-ci-multi-runner

