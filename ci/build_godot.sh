#!/bin/bash

[[ -d ~/godot/.git ]] || git clone https://github.com/godotengine/godot.git ~/godot

cd ~/godot

# Checkout latest stable
git checkout tags/2.0.3-stable

# Server for 64 bits Debug with Tools (CI)

scons -j 4 p=server target=release_debug tools=yes bits=64
mkdir -p ~/bin/
cp bin/godot_server.server.opt.tools.64 ~/bin/linux_server_tools_64

# Following is taken from http://docs.godotengine.org/en/latest/reference/batch_building_templates.html

# Remove eventual templates

rm -rf templates
mkdir -p templates

# Windows 32 Release and Debug

scons -j 4 p=windows target=release tools=no bits=32
cp bin/godot.windows.opt.32.exe templates/windows_32_release.exe
upx templates/windows_32_release.exe
scons -j 4 p=windows target=release_debug tools=no bits=32
cp bin/godot.windows.opt.debug.32.exe templates/windows_32_debug.exe
# upx templates/windows_32_debug.exe

# Windows 64 Release and Debug (UPX does not support it yet)

scons -j 4 p=windows target=release tools=no bits=64
cp bin/godot.windows.opt.64.exe templates/windows_64_release.exe
x86_64-w64-mingw32-strip templates/windows_64_release.exe
scons -j 4 p=windows target=release_debug tools=no bits=64
cp bin/godot.windows.opt.debug.64.exe templates/windows_64_debug.exe
x86_64-w64-mingw32-strip templates/windows_64_debug.exe

# Linux 64 Release and Debug

scons -j 4 p=x11 target=release tools=no bits=64
cp bin/godot.x11.opt.64 templates/linux_x11_64_release
upx templates/linux_x11_64_release
scons -j 4 p=x11 target=release_debug tools=no bits=64
cp bin/godot.x11.opt.debug.64 templates/linux_x11_64_debug
upx templates/linux_x11_64_debug

# Linux 32 Release and Debug

scons -j 4 p=x11 target=release tools=no bits=32
cp bin/godot.x11.opt.32 templates/linux_x11_32_release
upx templates/linux_x11_32_release
scons -j 4 p=x11 target=release_debug tools=no bits=32
cp bin/godot.x11.opt.debug.32 templates/linux_x11_32_debug
upx templates/linux_x11_32_debug

# Server for 32 and 64 bits (always in debug)
scons -j 4 p=server target=release_debug tools=no bits=64
cp bin/godot_server.server.opt.debug.64 templates/linux_server_64
upx templates/linux_server_64
scons -j 4 p=server target=release_debug tools=no bits=32
cp bin/godot_server.server.opt.debug.32 templates/linux_server_32
upx templates/linux_server_32


# Install templates

mkdir -p ~/.godot/templates
cp templates/* ~/.godot/templates

